<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Quiz
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje kvíz přiřazený k bodu v trase.
 */
class Quiz extends Model
{
    use HasFactory;

    public function answers()
    {
        return $this->hasMany(Quiz_Answer::class);
    }
}
