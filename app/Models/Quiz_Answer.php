<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Quiz_Answer
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje odpověď přiřazenou ke kvízu.
 */
class Quiz_Answer extends Model
{
    use HasFactory;

    protected $table = 'quiz_answers';

    public static function getOnlyCorrectAnswer($id): Collection
    {
        return Quiz_Answer::select('correct')
            ->where('id', $id)
            ->where('correct', '=', 1)
            ->get();
    }

    public static function getAllCorrectAnswers($quiz_id): Collection
    {
        return Quiz_Answer::where('quiz_id', '=', $quiz_id)
            ->where('correct', '=', 1)
            ->get();
    }
}
