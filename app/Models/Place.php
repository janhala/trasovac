<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Place
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje místo.
 */
class Place extends Model
{
    use HasFactory;

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function getPlaces($user_id): Collection
    {
        return Place::where('user_id', '!=', $user_id)
            ->get();
    }
}
