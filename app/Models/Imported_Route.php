<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Imported_Route
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje záznam o importované trase, a to pomocí odkazu, nebo číselného kódu.
 */
class Imported_Route extends Model
{
    use HasFactory;

    protected $table = 'imported_routes';

    public static function importedRoutesByUser($user_id): Collection
    {
        return DB::table('imported_routes')
            ->join('routes', 'imported_routes.route_id', '=', 'routes.id')
            ->select('routes.*', 'imported_routes.created_at')
            ->where('imported_routes.user_id', $user_id)
            ->get();
    }

    public static function existingImportedRouteByUser($user_id, $route_id): Collection
    {
        return DB::table('imported_routes')
            ->join('routes', 'imported_routes.route_id', '=', 'routes.id')
            ->select('routes.*', 'imported_routes.created_at')
            ->where('imported_routes.user_id', $user_id)
            ->where('imported_routes.route_id', $route_id)
            ->get();
    }
}
