<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Visited_Point
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje záznam o uživatelem navštíveném bodu na trase.
 */
class Visited_Point extends Model
{
    use HasFactory;

    protected $table = 'visited_points';

    public static function lastVisitedPoint($user_id, $route_id): Collection
    {
        return DB::table('visited_points')
            ->where('visited_points.user_id', $user_id)
            ->where('visited_points.route_id', $route_id)
            ->join('points', 'visited_points.point_id', '=', 'points.id')
            ->select('points.*')
            ->orderBy('points.id', 'DESC')
            ->get();
    }
}
