<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Teamwork
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje záznam o spolupráci uživatele na trase.
 */
class Teamwork extends Model
{
    use HasFactory;

    protected $table = 'teamwork';

    public static function allTeamworksWithUser($user_id): Collection
    {
        return DB::table('teamwork')
            ->join('routes', 'teamwork.route_id', '=', 'routes.id')
            ->select('routes.*', 'teamwork.created_at')
            ->where('teamwork.user_id', $user_id)
            ->get();
    }

    public static function existingTeamworkWithUser($user_id, $route_id): Collection
    {
        return DB::table('teamwork')
            ->join('routes', 'teamwork.route_id', '=', 'routes.id')
            ->select('routes.*', 'teamwork.created_at')
            ->where('teamwork.user_id', $user_id)
            ->where('teamwork.route_id', $route_id)
            ->get();
    }

    public static function teamworkWithUserOnRoute($user_id, $route_id): Collection
    {
        return DB::table('teamwork')
            ->select('teamwork.id')
            ->where('user_id', $user_id)
            ->where('route_id', $route_id)
            ->get();
    }
}
