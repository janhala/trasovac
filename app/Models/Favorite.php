<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Favorite
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje záznam o oblíbenosti trasy, či místa.
 */
class Favorite extends Model
{
    use HasFactory;

    public static function getByPlaceAndUser($user_id, $place_id): \Illuminate\Support\Collection
    {
        return DB::table('favorites')
            ->select('id')
            ->where('user_id', $user_id)
            ->where('place_id', $place_id)
            ->get();
    }

    public static function getByRouteAndUser($user_id, $route_id): \Illuminate\Support\Collection
    {
        return DB::table('favorites')
            ->select('id')
            ->where('user_id', $user_id)
            ->where('route_id', $route_id)
            ->get();
    }

    public static function deleteByPlaceAndUser($user_id, $place_id) {
        DB::table('favorites')
            ->where('user_id', $user_id)
            ->where('place_id', $place_id)
            ->delete();
    }

    public static function deleteByRouteAndUser($user_id, $route_id) {
        DB::table('favorites')
            ->where('user_id', $user_id)
            ->where('route_id', $route_id)
            ->delete();
    }

    public static function getUserFavoritePlaces($user_id): \Illuminate\Support\Collection
    {
        return DB::table('favorites')
            ->where('favorites.user_id', $user_id)
            ->join('places', 'favorites.place_id', '=', 'places.id')
            ->select('places.*', 'favorites.created_at')
            ->get();
    }

    public static function getUserFavoriteRoutes($user_id): \Illuminate\Support\Collection
    {
        return DB::table('favorites')
            ->where('favorites.user_id', $user_id)
            ->join('routes', 'favorites.route_id', '=', 'routes.id')
            ->select('routes.*', 'favorites.created_at')
            ->get();
    }
}
