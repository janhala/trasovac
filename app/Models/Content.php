<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Content
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje obsah přiřazený k bodu v trase.
 */
class Content extends Model
{
    use HasFactory;
}
