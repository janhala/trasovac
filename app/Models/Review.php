<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje recenzi tras, či místa.
 */
class Review extends Model
{
    use HasFactory;

    public function users()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
