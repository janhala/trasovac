<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Item
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje předmět, který byl přidán do trasy.
 */
class Item extends Model
{
    use HasFactory;

    public static function getRouteItems($route_id): Collection
    {
        return Item::where('route_id', $route_id)
            ->orderBy('items.required', 'DESC')
            ->get();
    }
}
