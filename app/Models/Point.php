<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Point
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje bod, který je přidán do trasy.
 */
class Point extends Model
{
    use HasFactory;

    public function contents()
    {
        return $this->hasMany(Content::class);
    }

    public function quiz()
    {
        return $this->hasOne(Quiz::class);
    }

    public function route()
    {
        return $this->hasOne(Route::class,'id', 'route_id');
    }

    public static function getAllWaypoints($route_id): Collection
    {
        return Point::select('lat', 'lng')
            ->where('route_id', $route_id)
            ->get();
    }

    public static function getAllPoints($route_id): Collection
    {
        return Point::where('route_id', $route_id)
            ->get();
    }

    public static function getStartPoint($route_id): Collection
    {
        return Point::where('route_id', $route_id)
            ->get();
    }

    public static function getNextPoint($route_id, $point_id): Collection
    {
        return Point::where('route_id', '=', $route_id)
            ->where('id', '>', $point_id)
            ->orderBy('points.id', 'ASC')
            ->get();
    }
}
