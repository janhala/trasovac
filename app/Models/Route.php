<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

/**
 * Class Route
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje trasu.
 */
class Route extends Model
{
    use HasFactory;

    public function items()
    {
        return $this->hasMany(Item::class);
    }

    public function points()
    {
        return $this->hasMany(Point::class);
    }

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public static function getRoutes($user_id): Collection
    {
        return Route::where('user_id','!=', $user_id)
            ->where('public', '=', '1')
            ->get();
    }

    public static function getRouteByURL($url): Collection
    {
        return Route::where('url', $url)
            ->get();
    }

    public static function getRouteByURLforTeamwork($url_for_teamwork): Collection
    {
        return Route::where('url_for_teamwork', $url_for_teamwork)
            ->get();
    }

    public static function getRouteByNumericCode($url_for_teamwork): Collection
    {
        return Route::where('url_for_teamwork', $url_for_teamwork)
            ->get();
    }
}
