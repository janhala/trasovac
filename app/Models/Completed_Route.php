<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Completed_Route
 * @package App\Models
 * @author Jan Hála
 * @license MIT
 * Model představuje trasu úspěšně projitou uživatelem.
 */
class Completed_Route extends Model
{
    use HasFactory;

    protected $table = 'completed_routes';

    public static function newRouteCompleted($user_id, $route_id)
    {
        $completed_route = new Completed_Route();
        $completed_route->user_id = $user_id;
        $completed_route->route_id = $route_id;
        $completed_route->save();
    }

    public static function allRoutesCompletedByUser($user_id)
    {
        return DB::table('completed_routes')
            ->select('*')
            ->where('user_id', $user_id)
            ->get();
    }

    public static function getCompletedRouteByUser($user_id, $route_id) {
        return DB::table('completed_routes')
            ->select('*')
            ->where('user_id', '=', $user_id)
            ->where('route_id', '=', $route_id)
            ->get();
    }
}
