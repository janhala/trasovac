<?php

namespace App\Http\Controllers;

use App\Models\Completed_Route;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class ReviewController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro správu recenzí míst a tras.
 */
class ReviewController extends Controller
{
    public function addOrUpdateReview(Request $request, $id = null) {
        $request->validate([
            'number_of_stars' => 'required|numeric',
            'comment' => 'required|string',
            'user_id' => 'numeric'
        ]);

        $route_id = $request->input('route_id');
        $place_id = $request->input('place_id');
        $user_id = Auth::id();

        $already_completed_route = Completed_Route::getCompletedRouteByUser($user_id, $route_id);
        if ($already_completed_route->isNotEmpty() && $route_id !== null || $place_id !== null) {
            if ($id !== null) {
                $review = Review::findOrFail($id);
            } else {
                $review = new Review();
            }

            $review->number_of_stars = $request->input('number_of_stars');
            $review->comment = $request->input('comment');
            if ($route_id) {
                $review->route_id = $route_id;
            } else if ($place_id) {
                $review->place_id = $place_id;
            }
            $review->user_id = $user_id;
            $review->save();

            if ($route_id) {
                Session::flash('message', 'Recenze trasy byla úspěšně přidána.');

                return redirect()->route('view_route', $route_id);
            } else if ($place_id) {
                Session::flash('message', 'Recenze místa byla úspěšně přidána.');

                return redirect()->route('view_place', $place_id);
            }
        } else {
            if ($route_id) {
                return redirect()->route('view_route', $route_id)->withErrors(['Nemáte práva pro napsání recenze pro tuto trasu.']);
            } else if ($place_id) {
                return redirect()->route('view_place', $place_id)->withErrors(['Nemáte práva pro napsání recenze pro toto místo.']);
            }
        }
    }
}
