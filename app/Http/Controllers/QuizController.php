<?php

namespace App\Http\Controllers;

use App\Models\Point;
use App\Models\Quiz;
use App\Models\Quiz_Answer;
use App\Models\Visited_Point;
use Illuminate\Http\Request;

/**
 * Class QuizController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro získávání uložených kvízu.
 * Je využit také pro kontrolu správnosti odpovědí kvízu zaslaných uživatelem.
 */
class QuizController extends Controller
{
    public function getQuiz(int $point_id) {
        $quiz = Point::find($point_id)->quiz;

        if (isset($quiz)) {
            $answers = Quiz::find($quiz->id)->answers;
            $quiz_with_answers = array();
            $quiz_with_answers['quiz'] = $quiz;
            $quiz_with_answers['answers'] = $answers;

            return $quiz_with_answers;
        } else {
            return "empty";
        }
    }

    public function checkAnswers(int $user_id, int $point_id, Request $request): array
    {
        $quiz = Point::find($point_id)->quiz;
        $selected_answers = $request->input('selected_answers');

        $response = array();
        $response['message'] = '';
        $response['data'] = array();

        if (isset($quiz)) {
            $wrong_answers_count = 0;
            $correct_answers_count = 0;
            $number_of_correct_answers = Quiz_Answer::getAllCorrectAnswers($quiz->id)->count();

            if ($selected_answers !== null) {
                foreach ($selected_answers as $selected_answer) {
                    $correct_answer = Quiz_Answer::getOnlyCorrectAnswer($selected_answer);

                    if ($correct_answer->isEmpty()) {
                        $wrong_answers_count++;
                    } else {
                        $correct_answers_count++;
                    }
                }

                if ($wrong_answers_count == 0 && $number_of_correct_answers == $correct_answers_count) {
                    $route_id = Point::findOrFail($point_id)->route_id;
                    $next_point = Point::getNextPoint($route_id, $point_id);

                    if ($next_point->isNotEmpty()) {
                        $response['data']['next_point_id'] = $next_point->first()->id;
                        $response['message'] = "completed correctly";
                    } else {
                        $response['message'] = "end of route";
                    }

                    if ($user_id != '0') {
                        $visited_point = new Visited_Point();
                        $visited_point->user_id = $user_id;
                        $visited_point->route_id = $route_id;
                        $visited_point->point_id = $point_id;
                        $visited_point->save();
                    }

                    return $response;
                } else {
                    $quiz = Point::find($point_id)->quiz;

                    if ($quiz->retake == 1) {
                        $response['message'] = "repeat quiz";
                    } else if ($quiz->hint_point_lat !== null && $quiz->hint_point_lng !== null && $quiz->hint !== null) {
                        $response['message'] = "go to the point";
                        $response['data']['lat'] = $quiz->hint_point_lat;
                        $response['data']['lng'] = $quiz->hint_point_lng;
                        $response['data']['hint'] = $quiz->hint;
                    } else if ($quiz->hint !== null) {
                        $response['message'] = "show hint";
                        $response['data']['hint'] = $quiz->hint;
                    }

                    return $response;
                }
            } else {
                $response['message'] = "no answers selected";

                return $response;
            }
        } else {
            $response['message'] = "quiz does not exist";

            return $response;
        }
    }
}
