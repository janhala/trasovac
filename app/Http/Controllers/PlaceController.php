<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use App\Models\Place;
use App\Models\Review;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

/**
 * Class PlaceController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro správu míst.
 */
class PlaceController extends Controller
{
    public function store(Request $request, int $id = null)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'lat' => ['required','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lng' => ['required','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],
            'address' => 'nullable|string|max:255',
            'entrance_fee' => 'nullable|numeric|min:0|max:10000'
        ]);

        if ($id !== null) {
            $place = Place::findOrFail($id);
            Session::flash('message', 'Místo bylo úspěšně aktualizováno.');
        } else {
            $place = new Place();
            Session::flash('message', 'Místo bylo úspěšně vytvořeno.');
        }
        $place->name = $request->input('name');
        $place->description = $request->input('description');
        $place->lat = $request->input('lat');
        $place->lng = $request->input('lng');
        if ($request->input('address')) {
            $place->address = $request->input('address');
        }
        if ($request->input('entrance_fee') || $request->input('entrance_fee') === "0") {
            $place->entrance_fee = $request->input('entrance_fee');
        } elseif ($request->input('entrance_fee') === null) {
            $place->entrance_fee = null;
        }
        $place->user_id = Auth::id();
        $place->save();

        return redirect()->route('view_place', ['id' => $place->id]);
    }

    public function delete(int $id) {
        Place::findOrFail($id)->delete();

        Session::flash('message', 'Místo bylo úspěšně smazáno.');

        return redirect()->route('my_places');
    }

    public function myPlaces() {
        $places = Place::where('user_id', Auth::id())->get();
        $places_count = $places->count();

        return view('places.my_places', ['places' => $places, 'places_count' => $places_count]);
    }

    public function view(int $id) {
        $place = Place::findOrFail($id);

        $favorite = Favorite::getByPlaceAndUser(Auth::id(), $id)->count();

        $reviews = Review::where('place_id', '=', $id)
            ->orderBy('number_of_stars', 'DESC')
            ->get();

        $review = Review::where('place_id', '=', $id)
            ->where('user_id', '=', Auth::id())
            ->get();

        return view('places.view_place', [
            'place' => $place,
            'favorite' => $favorite,
            'reviews' => $reviews,
            'review' => $review
        ]);
    }

    public function createOrUpdatePlace(int $id = null) {
        if ($id) {
            $place = Place::find($id);

            return view('places.create_or_update_place', ['place' => $place]);
        } else {
            return view('places.create_or_update_place');
        }
    }
}
