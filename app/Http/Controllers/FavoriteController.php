<?php

namespace App\Http\Controllers;

use App\Models\Favorite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * Class FavoriteController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro správu oblíbenosti tras a míst, stejně jako ke získávání tras a míst označených jako oblíbené.
 */
class FavoriteController extends Controller
{
    public function newFavoritePlace(Request $request): string
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'place_id' => 'required|numeric'
        ]);

        $favorite = new Favorite();
        $favorite->user_id = $request->input('user_id');;
        $favorite->place_id = $request->input('place_id');
        $favorite->save();

        return 'success';
    }

    public function removeFavoritePlace(Request $request): string
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'place_id' => 'required|numeric'
        ]);

        $user_id = $request->input('user_id');
        $place_id = $request->input('place_id');

        Favorite::deleteByPlaceAndUser($user_id, $place_id);

        return 'success';
    }

    public function getFavouriteRoutesAndPlaces() {
        $user_id = Auth::id();

        $favorite_routes = Favorite::getUserFavoriteRoutes($user_id);
        $favorite_routes_count = $favorite_routes->count();

        $favorite_places = Favorite::getUserFavoritePlaces($user_id);
        $favorite_places_count = $favorite_places->count();

        return view('favorites', [
            'places' => $favorite_places, 'places_count' => $favorite_places_count,
            'routes' => $favorite_routes, 'routes_count' => $favorite_routes_count
        ]);
    }

    public function newFavoriteRoute(Request $request): string
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'route_id' => 'required|numeric'
        ]);

        $favorite = new Favorite();
        $favorite->user_id = $request->input('user_id');;
        $favorite->route_id = $request->input('route_id');
        $favorite->save();

        return 'success';
    }

    public function removeFavoriteRoute(Request $request): string
    {
        $request->validate([
            'user_id' => 'required|numeric',
            'route_id' => 'required|numeric'
        ]);

        $user_id = $request->input('user_id');
        $route_id = $request->input('route_id');

        Favorite::deleteByRouteAndUser($user_id, $route_id);

        return 'success';
    }
}
