<?php

namespace App\Http\Controllers;

use App\Models\Point;

/**
 * Class ContentController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro získání obsahu přiřazeného k bodům, které se nacházejí v jednotlivých trasách.
 */
class ContentController extends Controller
{
    public function getContents(int $point_id) {
        $contents = Point::find($point_id)->contents;

        if ($contents->isNotEmpty()) {
            return $contents;
        } else {
            return "empty";
        }
    }
}
