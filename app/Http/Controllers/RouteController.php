<?php

namespace App\Http\Controllers;

use App\Models\Completed_Route;
use App\Models\Content;
use App\Models\Favorite;
use App\Models\Imported_Route;
use App\Models\Item;
use App\Models\Place;
use App\Models\Point;
use App\Models\Quiz;
use App\Models\Quiz_Answer;
use App\Models\Review;
use App\Models\Route;
use App\Models\Teamwork;
use App\Models\Visited_Point;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

/**
 * Class RouteController
 * @package App\Http\Controllers
 * @author Jan Hála
 * @license MIT
 * Controller slouží pro správu tras, stejně jako ke získávání informací o nich.
 * Obsahuje také funkce zaštiťující navigaci po trasách.
 */
class RouteController extends Controller
{
    private $lat_from = null;
    private $lng_from = null;

    public function store(Request $request, int $id = null)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'difficulty' => 'required|numeric',
            'estimated_time' => 'required|numeric|min:1|max:10080',
            'public' => 'required|boolean',
            'items' => 'nullable',
            'points' => 'nullable'
        ]);

        if ($id !== null) {
            $route = Route::findOrFail($id);

            $existing_teamwork = Teamwork::teamworkWithUserOnRoute(Auth::id(), $route->id);
            if ($route->user_id !== Auth::id() && $existing_teamwork->isEmpty()) {
                return redirect()->route('my_routes')->withErrors(['Tuto trasu nemůžete editovat.']);
            }
        } else {
            $route = new Route();
            $route->url = null;
            $route->numeric_code = null;
            $route->url_for_teamwork = null;
        }

        $route->name = $request->input('name');
        $route->description = $request->input('description');
        $route->difficulty = $request->input('difficulty');
        $route->estimated_time = $request->input('estimated_time');
        $route->public = $request->input('public');

        if ((isset($existing_teamwork) && $existing_teamwork->isEmpty()) || $id === null) {
            $route->user_id = Auth::id();
        }

        if ($request->input('request_url') !== 'on' || $request->input('public') === "1") {
            $route->url = null;
        }

        if ($request->input('request_numeric_code') !== 'on' || $request->input('public') === "1") {
            $route->numeric_code = null;
        }

        if ($request->input('request_url_for_teamwork') !== 'on') {
            $route->url_for_teamwork = null;
        }

        $route->save();

        if ($request->input('request_url') === 'on' && $request->input('public') == 0 && $route->url === null) {
            $this->generateNewRouteURL($route->id);
        }

        if ($request->input('request_numeric_code') === 'on' && $request->input('public') == 0 && $route->numeric_code === null) {
            $this->generateNewRouteNumericCode($route->id);
        }

        if ($request->input('request_url_for_teamwork') === 'on' && $route->url_for_teamwork === null) {
            $this->generateNewRouteURLforTeamWork($route->id);
        }

        if ($id !== null) {
            foreach ($route->items as $item) {
                $item->delete();
            }
        }

        if ($request->input('items')) {
            $items = json_decode($request->input('items'));

            foreach ($items as $item_obj) {
                $item = new Item();
                $item->name = $item_obj->name;
                $item->required = $item_obj->required;
                $item->route_id = $route->id;
                $item->save();
            }
        }

        $distance = 0;

        if ($id !== null) {
            foreach ($route->points as $point) {
                $point->delete();
            }
        }

        if ($request->input('points')) {
            $points = json_decode($request->input('points'));

            foreach ($points as $point_obj) {
                if ($this->lat_from !== null && $this->lng_from !== null) {
                    $distance += $this->calculateDistance($this->lat_from, $this->lng_from, $point_obj->lat, $point_obj->lng);
                }

                $this->lat_from = $point_obj->lat;
                $this->lng_from = $point_obj->lng;

                $point_original_id = $point_obj->id;

                $point = new Point();
                $point->name = $point_obj->name;
                $point->description = $point_obj->description;
                $point->lat = $point_obj->lat;
                $point->lng = $point_obj->lng;
                if (isset($point_obj->address)) {
                    $point->address = $point_obj->address;
                }

                if (isset($point_obj->entrance_fee) && $point_obj->entrance_fee !== '') {
                    $point->entrance_fee = $point_obj->entrance_fee;
                }
                $point->route_id = $route->id;
                $point->save();

                if ( ($id !== null || $request->hasFile('content_image_'.$point_original_id)) && $request->has('content_image_name_'.$point_original_id)) {
                    if ($id === null) {
                        $file = $request->file('content_image_'.$point_original_id);
                        $file_name = 'image_'.$point->id.'.'.$file->getClientOriginalExtension();

                        $file->storeAs('public', $file_name, 'local');
                    } else {
                        if ($request->has('existing_content_image_id_'.$point_original_id) && $request->hasFile('content_image_'.$point_original_id)) {
                            Storage::delete('public/'.$request->input('existing_content_image_id_'.$point_original_id));

                            $file = $request->file('content_image_'.$point_original_id);
                            $file_name = 'image_'.$point->id.'.'.$file->getClientOriginalExtension();

                            $file->storeAs('public', $file_name, 'local');
                        } else if ($request->has('existing_content_image_id_'.$point_original_id)) {
                            $file_name = $request->input('existing_content_image_id_'.$point_original_id);
                        } else {
                            $file = $request->file('content_image_'.$point_original_id);
                            $file_name = 'image_'.$point->id.'.'.$file->getClientOriginalExtension();

                            $file->storeAs('public', $file_name, 'local');
                        }
                    }

                    if ($file_name !== null) {
                        $content_image = new Content();
                        $content_image->name = $request->input('content_image_name_'.$point_original_id);
                        $content_image->type = 1;
                        $content_image->file = $file_name;
                        $content_image->point_id = $point->id;
                        $content_image->save();
                    }
                } else if ($id !== null && $request->has('existing_content_image_id_'.$point_original_id)) {
                    Storage::delete('public/'.$request->input('existing_content_image_id_'.$point_original_id));
                }

                if ($request->has('content_text_'.$point_original_id) && $request->has('content_text_name_'.$point_original_id)) {
                    $content_text = new Content();
                    $content_text->name = $request->input('content_text_name_'.$point_original_id);
                    $content_text->type = 2;
                    $content_text->text = $request->input('content_text_'.$point_original_id);
                    $content_text->point_id = $point->id;
                    $content_text->save();
                }

                if ( ($id !== null || $request->hasFile('content_sound_'.$point_original_id)) && $request->has('content_sound_name_'.$point_original_id)) {
                    if ($id === null) {
                        $file = $request->file('content_sound_'.$point_original_id);
                        $file_name = 'sound_'.$point->id.'.'.$file->getClientOriginalExtension();

                        $file->storeAs('public', $file_name, 'local');
                    } else {
                        if ($request->has('existing_content_sound_id_'.$point_original_id) && $request->hasFile('content_sound_'.$point_original_id)) {
                            Storage::delete('public/'.$request->input('existing_content_sound_id_'.$point_original_id));

                            $file = $request->file('content_sound_'.$point_original_id);
                            $file_name = 'sound_'.$point->id.'.'.$file->getClientOriginalExtension();

                            $file->storeAs('public', $file_name, 'local');
                        } else if ($request->has('existing_content_sound_id_'.$point_original_id)) {
                            $file_name = $request->input('existing_content_sound_id_'.$point_original_id);
                        } else {
                            $file = $request->file('content_sound_'.$point_original_id);
                            $file_name = 'sound_'.$point->id.'.'.$file->getClientOriginalExtension();

                            $file->storeAs('public', $file_name, 'local');
                        }
                    }

                    if ($file_name !== null) {
                        $content_sound = new Content();
                        $content_sound->name = $request->input('content_sound_name_'.$point_original_id);
                        $content_sound->type = 3;
                        $content_sound->file = $file_name;
                        $content_sound->point_id = $point->id;
                        $content_sound->save();
                    }
                } else if ($id !== null && $request->has('existing_content_sound_id_'.$point_original_id)) {
                    Storage::delete('public/'.$request->input('existing_content_sound_id_'.$point_original_id));
                }

                if ($request->has('quiz_name_'.$point_original_id) && $request->has('quiz_description_'.$point_original_id) && $request->has('quiz_question_'.$point_original_id)) {
                    $quiz = new Quiz();
                    $quiz->name = $request->input('quiz_name_'.$point_original_id);
                    $quiz->description = $request->input('quiz_description_'.$point_original_id);
                    $quiz->question = $request->input('quiz_question_'.$point_original_id);

                    if ($request->has('quiz_hint_'.$point_original_id) && $request->has('quiz_hint_point_lat_'.$point_original_id) && $request->has('quiz_hint_point_lng_'.$point_original_id)) {
                        $quiz->retake = 0;
                        $quiz->hint = $request->input('quiz_hint_'.$point_original_id);
                        $quiz->hint_point_lat = $request->input('quiz_hint_point_lat_'.$point_original_id);
                        $quiz->hint_point_lng = $request->input('quiz_hint_point_lng_'.$point_original_id);
                    } else if ($request->has('quiz_hint_'.$point_original_id)) {
                        $quiz->retake = 0;
                        $quiz->hint = $request->input('quiz_hint_'.$point_original_id);
                    } else {
                        $quiz->retake = 1;
                    }

                    $quiz->point_id = $point->id;

                    $quiz->save();

                    for ($x = 1; $x <= 4; $x++) {
                        if ($request->has('quiz_answer_'.$point_original_id.'_'.$x)) {
                            $quiz_answers = new Quiz_Answer();
                            $quiz_answers->answer = $request->input('quiz_answer_'.$point_original_id.'_'.$x);
                            $quiz_answers->quiz_id = $quiz->id;

                            if ($request->has('quiz_answer_right_'.$point_original_id.'_'.$x)) {
                                $quiz_answers->correct = 1;
                            } else {
                                $quiz_answers->correct = 0;
                            }

                            $quiz_answers->save();
                        }
                    }
                }
            }
        }

        $route = Route::findOrFail($route->id);
        $route->distance = $distance;

        $route->save();

        if (isset($id)) {
            Session::flash('message', 'Trasa byla úspěšně aktualizována.');
        } else {
            Session::flash('message', 'Trasa byla úspěšně vytvořena.');
        }

        return redirect()->route('view_route', $route->id);
    }

    public function delete(int $id) {
        $route = Route::findOrFail($id);

        foreach ($route->points as $point) {
            if ($point->contents->isNotEmpty()) {
                foreach ($point->contents as $content) {
                    if (($content->type == 1 || $content->type == 3) && $content->file !== null) {
                        Storage::delete('public/'.$content->file);
                    }
                }
            }
        }

        $route->delete();

        Session::flash('message', 'Trasa byla úspěšně smazána.');

        return redirect()->route('my_routes');
    }

    public function myRoutes() {
        $routes = Route::where('user_id', '=', Auth::id())->get();
        $routes_count = $routes->count();

        return view('routes.my_routes', ['routes' => $routes, 'routes_count' => $routes_count]);
    }

    public function view(int $id) {
        $route = Route::findOrFail($id);

        $logged_user_id = Auth::id();
        if ($logged_user_id !== null) {
            $favorite = Favorite::getByRouteAndUser($logged_user_id, $id)->count();

            $existing_teamwork = Teamwork::existingTeamworkWithUser(Auth::id(), $route->id);
            $imported_route = Imported_Route::existingImportedRouteByUser(Auth::id(), $route->id);
            if ($route->public !== 1) {
                if ($existing_teamwork->isEmpty() && $imported_route->isEmpty() && $logged_user_id !== $route->user_id) {
                    return redirect()->route('browse_routes')->withErrors(['Trasa je označena jako neveřejná a nemáte práva k jejímu načtení.']);
                }
            }
        } else {
            $favorite = null;

            if ($route->public !== 1) {
                return redirect()->route('browse_routes')->withErrors(['Trasa je označena jako neveřejná a nemáte práva k jejímu načtení.']);
            }
        }

        $waypoints = Point::getAllWaypoints($id);
        $start_point = $waypoints->first()->lat.','.$waypoints->first()->lng;
        $end_point = $waypoints->last()->lat.','.$waypoints->last()->lng;
        $waypoints_between = $waypoints->forget(0);
        $waypoints_between = $waypoints_between->slice(0, -1);
        $waypoints_between_array = [];
        foreach ($waypoints_between as $waypoint_between) {
            array_push($waypoints_between_array, $waypoint_between->lat.','.$waypoint_between->lng);
        }

        $items = Item::getRouteItems($id);

        $points = Point::getAllPoints($id);

        $alphabet = ['A','B','C','D','E','F','G','H','I','J'];

        $reviews = Review::where('route_id', '=', $id)
                    ->orderBy('number_of_stars', 'DESC')
                    ->get();

        if ($logged_user_id !== null) {
            $already_completed_route = Completed_Route::getCompletedRouteByUser($logged_user_id, $id);

            $existing_teamwork = Teamwork::teamworkWithUserOnRoute($logged_user_id, $route->id);
            if ($existing_teamwork->isNotEmpty()) {
                $existing_teamwork = 1;
            } else {
                $existing_teamwork = 0;
            }
        } else {
            $already_completed_route = null;
            $existing_teamwork = 0;
        }

        $review = Review::where('route_id', '=', $id)
            ->where('user_id', '=', Auth::id())
            ->get();

        return view('routes.view_route',
            [
                'route' => $route,
                'favorite' => $favorite,
                'start_point' => $start_point,
                'end_point' => $end_point,
                'waypoints_between' => json_encode($waypoints_between_array),
                'items' => $items,
                'points' => $points,
                'alphabet' => $alphabet,
                'reviews' => $reviews,
                'already_completed_route' => $already_completed_route,
                'existing_teamwork' => $existing_teamwork,
                'review' => $review
            ]
        );
    }

    public function createOrUpdateRoute(int $id = null) {
        if ($id) {
            $route = Route::findOrFail($id);

            $existing_teamwork = Teamwork::teamworkWithUserOnRoute(Auth::id(), $route->id);
            if ($route->user_id !== Auth::id() && $existing_teamwork->isEmpty()) {
                return redirect()->route('my_routes')->withErrors(['Tuto trasu nemůžete editovat.']);
            }

            $items = json_encode($route->items);
            $points = json_encode($route->points);
            $quizzes = array();
            $contents = array();
            foreach ($route->points as $point) {
                if ($point->quiz !== null) {
                    array_push($quizzes, $point->quiz);
                }

                foreach ($point->contents as $content) {
                    if ($content !== null) {
                        array_push($contents, $content);
                    }
                }
            }

            return view('routes.create_or_update_route', [
                'route' => $route, 'items' => $items, 'points' => $points,
                'quizzes' => $quizzes, 'contents' => $contents
            ]);
        } else {
            return view('routes.create_or_update_route');
        }
    }

    public function generateNewRouteURL($id) {
        $url = uniqid();

        $route_with_url = Route::getRouteByURL($url);

        if ($route_with_url->isEmpty()) {
            $route = Route::findOrFail($id);
            $route->url = $url;
            $route->save();
        }
    }

    public function generateNewRouteURLforTeamWork($id) {
        $url_for_teamwork = uniqid();

        $route_with_url = Route::getRouteByURLforTeamwork($url_for_teamwork);

        if ($route_with_url->isEmpty()) {
            $route = Route::findOrFail($id);
            $route->url_for_teamwork = $url_for_teamwork;
            $route->save();
        }
    }

    public function generateNewRouteNumericCode($id) {
        $numeric_code = mt_rand(100000, 999999);

        $route_with_num_code = Route::getRouteByNumericCode($numeric_code);

        if ($route_with_num_code->isEmpty()) {
            $route = Route::findOrFail($id);
            $route->numeric_code = $numeric_code;
            $route->save();
        }
    }

    public function startNavigation(int $id, int $current_point_id = null) {
        $last_visited = Visited_Point::lastVisitedPoint(Auth::id(), $id);

        if ($current_point_id !== null) {
            if ($current_point_id != '0') {
                $current_point = Point::find($current_point_id);

                if ($current_point !== null) {
                    $point = $current_point;
                };
            } else {
                $start_point = Point::getStartPoint($id);

                if ($start_point->isNotEmpty()) {
                    $point = $start_point->first();
                }
            }
        } elseif ($last_visited->isEmpty()) {
            $start_point = Point::getStartPoint($id);

            if ($start_point->isNotEmpty()) {
                $point = $start_point->first();
            }
        } else {
            $last_visited = $last_visited->first();
            $point_id = $last_visited->id;
            $route_id = Point::findOrFail($point_id)->route_id;
            $current_point = Point::getNextPoint($route_id, $point_id);

            if ($current_point->isNotEmpty()) {
                $point = $current_point->first();
            } else {
                $user_id = Auth::id();
                $already_completed_route = Completed_Route::getCompletedRouteByUser($user_id, $route_id);

                if ($already_completed_route->isEmpty()) {
                    Completed_Route::newRouteCompleted($user_id, $route_id);
                }

                $completed_routes_count = Completed_Route::allRoutesCompletedByUser($user_id)->count();
                $routes_count = Route::all()->count();
                $remaining_routes = ($routes_count-$completed_routes_count);

                Visited_Point::where('route_id', '=', $route_id)
                    ->where('user_id', '=', Auth::id())
                    ->delete();

                $author = Route::findOrFail($route_id)->author;

                return view('routes.congrats',
                    [
                        'route_id' => $route_id,
                        'routes_count' => $routes_count,
                        'completed_routes_count' => $completed_routes_count,
                        'remaining_routes' => $remaining_routes,
                        'author' => $author
                    ]
                );
            }
        }

        return view('routes.navigation',
            [
                'point' => $point
            ]
        );
    }

    public function arrivedToPoint(int $user_id, int $point_id) {
        $route_id = Point::findOrFail($point_id)->route_id;

        if ($user_id != '0') {
            $visited_point = new Visited_Point();
            $visited_point->user_id = $user_id;
            $visited_point->route_id = $route_id;
            $visited_point->point_id = $point_id;
            $visited_point->save();
        }

        $next_point = Point::getNextPoint($route_id, $point_id);

        if ($user_id == '0' && $next_point->isNotEmpty()) {
            return $next_point->first()->id;
        } elseif ($next_point->isNotEmpty()) {
            return 'point saved';
        } else {
            return 'end of route';
        }
    }

    public function importedRoutes() {
        $imported_routes = Imported_Route::importedRoutesByUser(Auth::id());
        $imported_routes_count = $imported_routes->count();

        $teamwork_routes = Teamwork::allTeamworksWithUser(Auth::id());
        $teamwork_routes_count = $teamwork_routes->count();

        return view('routes.imported_routes', [
            'imported_routes' => $imported_routes,
            'imported_routes_count' => $imported_routes_count,
            'teamwork_routes' => $teamwork_routes,
            'teamwork_routes_count' => $teamwork_routes_count
        ]);
    }

    public function importRouteUsingLink(string $url, int $route_id) {
        $route = Route::where('id', '=', $route_id)
            ->where('url', '=', $url)
            ->where('user_id', '!=', Auth::id())
            ->get()
            ->first();

        if ($route !== null) {
            $already_imported = Imported_Route::existingImportedRouteByUser(Auth::id(), $route_id);

            if ($already_imported->isEmpty()) {
                $imported_route = new Imported_Route();
                $imported_route->route_id = $route->id;
                $imported_route->user_id = Auth::id();
                $imported_route->save();

                Session::flash('message', 'Trasa byla úspěšně importována pomocí odkazu.');

                return redirect()->route('imported_routes');
            } else {
                return redirect()->route('imported_routes')->withErrors(['Tuto trasu jste již importoval.']);
            }
        } else {
            return redirect()->route('imported_routes')->withErrors(['Pomocí tohoto odkazu nelze importovat žádnou trasu, případně jste tvůrcem trasy.']);
        }
    }

    public function importRouteUsingNumericCode(Request $request) {
        $request->validate([
            'numeric_code' => 'required|numeric|digits:6'
        ]);

        $numeric_code = $request->input('numeric_code');

        $route = Route::where('numeric_code', '=', $numeric_code)
            ->where('user_id', '!=', Auth::id())
            ->get()
            ->first();

        if ($route !== null) {
            $already_imported = Imported_Route::existingImportedRouteByUser(Auth::id(), $route->id);

            if ($already_imported->isEmpty()) {
                $imported_route = new Imported_Route();
                $imported_route->route_id = $route->id;
                $imported_route->user_id = Auth::id();
                $imported_route->save();

                Session::flash('message', 'Trasa byla úspěšně importována pomocí číselného kódu.');

                return redirect()->route('imported_routes');
            } else {
                return redirect()->route('imported_routes')->withErrors(['Tuto trasu jste již importoval.']);
            }
        } else {
            return redirect()->route('imported_routes')->withErrors(['Pomocí zadaného číselného kódu nelze importovat žádnou trasu, případně jste tvůrcem trasy.']);
        }
    }

    public function importRouteForTeamwork(string $url, int $route_id) {
        $route = Route::where('id', '=', $route_id)
            ->where('url_for_teamwork', '=', $url)
            ->where('user_id', '!=', Auth::id())
            ->get()
            ->first();

        if ($route !== null) {
            $already_imported = Teamwork::existingTeamworkWithUser(Auth::id(), $route_id);

            if ($already_imported->isEmpty()) {
                $teamwork = new Teamwork();
                $teamwork->route_id = $route->id;
                $teamwork->user_id = Auth::id();
                $teamwork->save();

                Session::flash('message', 'Trasa určená pro spolupráci byla úspěšně importována pomocí odkazu.');

                return redirect()->route('imported_routes', ['#teamwork']);
            } else {
                return redirect()->route('imported_routes', ['#teamwork'])->withErrors(['Na této trase již spolupracujete.']);
            }
        } else {
            return redirect()->route('imported_routes', ['#teamwork'])->withErrors(['Přes tento odkaz nelze importovat žádnou spolupráci, případně jste tvůrcem trasy.']);
        }
    }

    function calculateDistance($lat1, $lon1, $lat2, $lon2) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            return $dist * 60 * 1.1515 * 1.609344;
        }
    }

    public function browseRoutes() {
        $user_id = Auth::id();

        $routes = Route::getRoutes($user_id);
        $routes_count = $routes->count();

        $places = Place::getPlaces($user_id);
        $places_count = $places->count();

        return view('browse', [
            'routes' => $routes,
            'routes_count' => $routes_count,
            'places' => $places,
            'places_count' => $places_count
        ]);
    }

    public function browseRoutesWithFilters(Request $request) {
        $request->validate([
            'difficulty' => 'nullable|numeric|min:0|max:5',
            'distance_min' => 'nullable|numeric|min:1',
            'distance_max' => 'nullable|numeric|min:1',
            'estimated_time_min' => 'nullable|numeric|min:1',
            'estimated_time_max' => 'nullable|numeric|min:1',
            'current_lat' => ['nullable','regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'current_lng' => ['nullable','regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ]);

        $difficulty = $request->input('difficulty');
        $distance_min = $request->input('distance_min');
        $distance_max = $request->input('distance_max');
        $estimated_time_min = $request->input('estimated_time_min');
        $estimated_time_max = $request->input('estimated_time_max');
        $current_lat = $request->input('current_lat');
        $current_lng = $request->input('current_lng');

        $routes_query = DB::table('routes');
        $user_id = Auth::id();

        if ($difficulty !== null && $difficulty !== '0') {
            $routes_query->where('difficulty', '=', $difficulty);
        }

        if ($distance_min !== null) {
            $routes_query->where('distance', '>', $distance_min);
        }

        if ($distance_max !== null) {
            $routes_query->where('distance', '<', $distance_max);
        }

        if ($estimated_time_min !== null) {
            $routes_query->where('estimated_time', '>', $estimated_time_min);
        }

        if ($estimated_time_max !== null) {
            $routes_query->where('estimated_time', '<', $estimated_time_max);
        }

        $routes_query->where('user_id', '!=', $user_id)->where('public', '=', 1);

        $routes = $routes_query->get();
        $routes_count = $routes->count();


        if ($current_lat !== null && $current_lng !== null) {
            foreach ($routes as $route) {
                $start_point = Point::getStartPoint($route->id)->first();

                if ($start_point !== null) {
                    $route->distance_from_user = round($this->calculateDistance($start_point->lat, $start_point->lng, $current_lat, $current_lng));
                }
            }

            $routes = $routes->sortBy('distance_from_user');
        }

        $places = Place::getPlaces($user_id);
        $places_count = $places->count();

        return view('browse', [
            'routes' => $routes,
            'routes_count' => $routes_count,
            'places' => $places,
            'places_count' => $places_count,
            'difficulty' => $difficulty,
            'distance_min' => $distance_min,
            'distance_max' => $distance_max,
            'estimated_time_min' => $estimated_time_min,
            'estimated_time_max' => $estimated_time_max,
            'current_lat' => $current_lat,
            'current_lng' => $current_lng
        ]);
    }
}
