<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizzesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->text('description');
            $table->string('question', 255);
            $table->boolean('retake')->nullable();
            $table->text('hint')->nullable();
            $table->float('hint_point_lat', 11, 7)->nullable();
            $table->float('hint_point_lng', 11, 7)->nullable();
            $table->integer('point_id')->unsigned();
            $table->timestamps();
            $table->foreign('point_id')->references('id')->on('points')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
