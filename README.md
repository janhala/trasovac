<h1>Trasovač</h1>
<p><q>Tvořte a sdílejte turisticky zajímavá místa a trasy.</q></p>

## Stěžejní funkcionalita aplikace

- Vytváření tras a míst a jejich sdílení.
- Trasy lze označit jako neveřejné a sdílet pomocí speciálního odkazu či číselného kódu.
- U tras je možné povolit spolupráci a zaslat je svým spolupracovníkům, kteří je poté mohou editovat.
- Trasy lze procházet pomocí navigace po trase.
- K jednotlivým bodům trasy je možné přidávat obsah (obrázek, text, zvuk), zobrazující se při navštívení bodu.
- K bodům je také možné přiřadit kvíz ověřující znalosti uživatele procházejícího trasu.

## Aplikace na hostingu

Aplikace je provozována na webovém hostingu a je dostupná pomocí URL adresy [trasovac.com](https://trasovac.com/).

## Provozování aplikace na lokálním prostředí

Pro provozování aplikace je nutné nejprve naklonovat kód z tohoto repozitáře, a to do vhodného umístění ve vašem PC.
Dále je nutné mít nainstalované některé vývojové prostředí pro jazyk PHP - doporučen je [XAMPP](https://www.apachefriends.org/index.html),  a to ve verzi 8.0.3, případně jiné vývojové prostředí obsahující PHP ve verzi 8, MariaDB ve verzi 10.4 a phpMyAdmin ve verzi 5.1.0.

Nezbytný pro instalaci a správu závislostí aplikace je [Composer](https://getcomposer.org/) - aplikace Trasovač byla vyvinuta za využití Composeru ve verzi 2.0.11. Nutná je také instalace správce balíčku NPM, který je obsažen v JS modulu [Node.js](https://nodejs.org/en/) - ten byl při vývoji využit ve verzi 15.12.0.

Po naklonování repozitáře je třeba v kořenové složce projektu spustit následující příkaz,
díky kterému budou nainstalovány potřebné závislosti:

```
composer install
```

Poté je vhodné také nainstalovat další potřebné závislosti, a to pomocí NPM:

```
npm install
```

Pro kompilaci a minifikaci CSS a JavaScript souborů aplikace Trasovač se využívá také NPM:

```
npm run dev && npm run prod
```

Následně je nutné vytvořit v kořenovém adresáři projektu soubor s názvem ```.env``` - inspirovat se lze souborem ```.env.example```.
Po jeho vytvoření je nutné vytvořit také novou databázi (například pomocí nástroje PhpMyAdmin)
a v souboru ```.env``` je poté nutné vyplnit následující parametry, díky kterým je pak možné, aby se aplikace připojila k vytvořené databázi (hodnoty jsou pouze ukázkové):

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=trasovac
DB_USERNAME=root
DB_PASSWORD=
```

Pokud chcete používat například obnovu hesla, je nutné vyplnit v souboru ```.env``` i parametry začínající na ```MAIL_```.

Po vytvoření databáze je možné provést migrace - a to pomocí následujícího příkazu:

```
php artisan migrate
```

<p style="font-style: italic;">TIP: Pokud budete chtít během používání aplikace provést migrace znovu, a tím všechny data v databázi smazat, použijte následující příkaz:</p>

```
php artisan migrate:refresh
```

Poté je také potřeba vytvořit link směřující z úložiště aplikace do složky ```public```, a to pomocí následujícího příkazu:

```
php artisan storage:link
```
Na závěr je třeba vytvořit šifrovací klíč aplikace, a to pomocí následujícího příkazu:

```
php artisan key:generate
```

Nakonec následujícím příkazem aplikaci spustěte:

```
php artisan serve
```

Přes adresní řádek prohlížeče načtete adresu ```http://127.0.0.1:8000/``` a aplikaci můžete začít hned poté používat.
Je však samozřejmě nutné spustit i databázový server MySQL, a to například pomocí již zmíněného nástroje XAMPP.

Při používání aplikace se doporučuje využívat nejnovějších dostupných verzí moderních webových prohlížečů.

## Použité komponenty třetích stran

- **[Material Design for Bootstrap v4](https://mdbootstrap.github.io/bootstrap-material-design/docs/4.0/about/overview/)** (MIT)
- **[SnackBar](https://www.polonel.com/snackbar/)** (MIT)

## Autor aplikace

Autorem aplikace je [Jan Hála](https://www.linkedin.com/in/janhala/).

## Chyby a připomínky

Pokud v aplikaci objevíte jakoukoliv chybu nebo budete mít návrhy na její vylepšení, konktaktujte prosím autora aplikace pomocí emailové adresy [honza.hala@gmail.com](mailto:honza.hala@gmail.com).

## Licence

Aplikace Trasovač byla vyvinuta pod [licencí MIT](https://opensource.org/licenses/MIT).
