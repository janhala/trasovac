@extends('layouts.app')

@section('title', 'Procházet')

@section('head')
    <style>
        #current_location {
            width: 95%;
            height: 35px;
        }

        #current_location_icon {
            margin-left: -30px;
            margin-top: 5px;
            cursor: pointer;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <ul class="nav nav-tabs bg-primary w-100">
            <li class="nav-item mx-auto">
                <a class="nav-link active" href="#routes" id="routes_btn">TRASY</a>
            </li>
            <li class="nav-item mx-auto">
                <a class="nav-link" href="#places" id="places_btn">MÍSTA</a>
            </li>
        </ul>
    </div>

    <div class="row" id="routes">
        <div class="container">
            <div class="container pt-3">
                <div class="row">
                    <div class="col-sm">
                        <div class="form-group">
                            <label for="current_location" class="bmd-label-static">Současná poloha</label>
                            <div class="row">
                                <input type="text" class="form-control" id="current_location">
                                <span id="current_location_icon" class="material-icons">my_location</span>
                            </div>
                            <span class="bmd-help">
                                Zadejte svou polohu - trasy a místa se poté seřadí podle své vzdálenosti od ní.
                            </span>
                        </div>
                    </div>
                    <div class="col-sm mt-4">
                        <button type="submit" class="btn btn-primary btn-raised align-middle" id="save_current_location_btn">
                            Uložit polohu
                        </button>
                        <button type="button" class="btn btn-primary btn-raised align-middle ml-2" data-toggle="modal" data-target="#change_filters_modal">
                            <i class="fas fa-filter"></i> Filtrovat
                        </button>
                    </div>
                </div>
            </div>

            <hr />

            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <div class="col-auto mt-3">
                <span>Aktuálně zobrazeno tras: {{$routes_count}}</span>
            </div>


            <div class="list-group">
                @foreach($routes as $route)
                    <a href="{{route('view_route', $route->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($route->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($route->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($route->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-chart-line"></i>
                                        {{$route->difficulty}}
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-route"></i>
                                        cca {{$route->distance}} km
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-stopwatch"></i>
                                        @if((floor($route->estimated_time/60)))
                                            @if ((floor($route->estimated_time/60)) > 0)
                                                {{(floor($route->estimated_time/60))}} hod.
                                            @endif
                                            @if (($route->estimated_time%60) > 0)
                                                {{($route->estimated_time%60)}} min.
                                            @endif
                                        @else
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    </span>
                            </div>
                        </div>
                        @if(isset($route->distance_from_user))
                            <div class="w-100">
                                <p class="float-left">
                                    <small class="pt-1 text-muted">vzdálenost od současné polohy: {{ $route->distance_from_user }} km</small>
                                </p>
                            </div>
                        @endif
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row" id="places">
        <div class="container">
            <div class="col-auto mt-3">
                <span>Zobrazeno míst: {{$places_count}}</span>
            </div>

            <div class="list-group">
                @foreach($places as $place)
                    <a href="{{route('view_place', $place->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($place->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($place->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($place->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            @if ($place->address)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-map-marker-alt"></i>
                                        {{ \Illuminate\Support\Str::limit($place->address, 25, $end='...') }}
                                    </span>
                                </div>
                            @endif

                            @if ($place->entrance_fee)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="far fa-money-bill-alt"></i>
                                        {{$place->entrance_fee}}
                                    </span>
                                </div>
                            @endif
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <!-- change_filters_modal -->
    <div class="modal fade" id="change_filters_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form id="change_filters_form" method="POST" action="{{ route('browse_routes_with_filters') }}">
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title">Filtrování tras</h5>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="difficulty" class="bmd-label-static">Obtížnost trasy</label>
                            <select class="form-control" id="difficulty" name="difficulty">
                                <option value="0" @if (isset($difficulty) && $difficulty === "0") selected @endif>všechny obtížnosti</option>
                                <option value="1" @if (isset($difficulty) && $difficulty === "1") selected @endif>1 (nejnižší)</option>
                                <option value="2" @if (isset($difficulty) && $difficulty === "2") selected @endif>2</option>
                                <option value="3" @if (isset($difficulty) && $difficulty === "3") selected @endif>3</option>
                                <option value="4" @if (isset($difficulty) && $difficulty === "4") selected @endif>4</option>
                                <option value="5" @if (isset($difficulty) && $difficulty === "5") selected @endif>5 (nejvyšší)</option>
                            </select>
                        </div>

                        <input type="hidden" id="current_lat" name="current_lat" @if (isset($current_lat)) value="{{$current_lat}}" @endif>
                        <input type="hidden" id="current_lng" name="current_lng" @if (isset($current_lng)) value="{{$current_lng}}" @endif>

                        <div class="form-group">
                            <label for="distance_min" class="bmd-label-static">Délka trasy minimální (v km)</label>
                            <input type="number" class="form-control" name="distance_min" id="distance_min" @if (isset($distance_min)) value="{{$distance_min}}" @endif>
                            <small class="text-muted">Zadejte minimální požadovanou délku trasy.</small>
                        </div>

                        <div class="form-group">
                            <label for="distance_max" class="bmd-label-static">Délka trasy maximální (v km)</label>
                            <input type="number" class="form-control" name="distance_max" id="distance_max" @if (isset($distance_max)) value="{{$distance_max}}" @endif>
                            <small class="text-muted">Zadejte maximální požadovanou délku trasy.</small>
                        </div>

                        <div class="form-group">
                            <label for="estimated_time_min" class="bmd-label-static">Čas potřebný k projití minimální (v min)</label>
                            <input type="number" class="form-control" name="estimated_time_min" id="estimated_time_min" @if (isset($estimated_time_min)) value="{{$estimated_time_min}}" @endif>
                            <small class="text-muted">Zadejte minimální potřebný čas k projití trasy, a to v minutách.</small>
                        </div>

                        <div class="form-group">
                            <label for="estimated_time_max" class="bmd-label-static">Čas potřebný k projití maximální (v min)</label>
                            <input type="number" class="form-control" name="estimated_time_max" id="estimated_time_max" @if (isset($estimated_time_max)) value="{{$estimated_time_max}}" @endif>
                            <small class="text-muted">Zadejte maximální potřebný čas k projití trasy, a to v minutách.</small>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                        <button type="submit" class="btn btn-primary">Filtrovat</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&libraries=places&callback=initAutocomplete">
    </script>

    <script>
        function initAutocomplete() {
            var input = document.getElementById('current_location');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.setFields(['geometry']);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (!place.geometry) {
                    window.alert("Žádné data o poloze '" + place.name + "' nebyla nalezena.");
                    return;
                }

                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();

                $("input[name=current_lat]").val(lat);
                $("input[name=current_lng]").val(lng);
            });
            autocomplete.setOptions({
                componentRestrictions: {country: ['CZ', 'SK']}
            });
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                Snackbar.show({
                    text: 'Tento prohlížeč nepodporuje geolokaci.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        }

        function showPosition(position) {
            var lat = position.coords.latitude.toFixed(7);
            var lng = position.coords.longitude.toFixed(7);

            localStorage.setItem('browse_current_location', lat + ', ' + lng);
            localStorage.setItem('browse_current_location_lat', lat);
            localStorage.setItem('browse_current_location_lng', lng);

            $( "#current_location" ).val(lat + ', ' + lng);
            $( "#current_lat" ).val(lat);
            $( "#current_lng" ).val(lng);
        }

        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    Snackbar.show({
                        text: 'Sdílení vaší polohy je blokováno. Povolte ho prosím, jinak nebudete moci pokračovat.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                    break;
                case error.POSITION_UNAVAILABLE:
                    Snackbar.show({
                        text: 'Informace o poloze nejsou k dispozici.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                    break;
                case error.TIMEOUT:
                    Snackbar.show({
                        text: 'Vypršel časový limit požadavku na získání polohy.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                    break;
                case error.UNKNOWN_ERROR:
                    Snackbar.show({
                        text: 'Nastala neznámá chyba.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                    break;
            }
        }

        $( document ).ready(function() {
            $( "#current_location_icon" ).click(function() {
                getLocation();
            });

            $( "#save_current_location_btn" ).click(function() {
                localStorage.setItem('browse_current_location', $( "#current_location" ).val());
                localStorage.setItem('browse_current_location_lat', $( "#current_lat" ).val());
                localStorage.setItem('browse_current_location_lng', $( "#current_lng" ).val());

                if ($( "#current_location" ).val() !== '' && $( "#current_lat" ).val() !== '' && $( "#current_lng" ).val() !== '') {
                    $( "#change_filters_form" ).submit();
                } else {
                    Snackbar.show({
                        text: 'Zadejte a vyberte prosím Vaší aktuální polohu.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                }
            });

            @if(isset($current_lat) && isset($current_lng))
            var browse_current_location = localStorage.getItem('browse_current_location');
            var browse_current_location_lat = localStorage.getItem('browse_current_location_lat');
            var browse_current_location_lng = localStorage.getItem('browse_current_location_lng');
            if (browse_current_location !== null && browse_current_location_lat !== null && browse_current_location_lng !== null) {
                $( "#current_location" ).val(browse_current_location);
                $( "#current_lat" ).val(browse_current_location_lat);
                $( "#current_lng" ).val(browse_current_location_lng);
            }
            @endif

            $(window).on('hashchange', function(){
                var hash = location.hash;

                if (hash === '#places') {
                    $("#routes_btn").removeClass("active");
                    $("#places_btn").addClass("active");

                    $( "#routes" ).hide();
                    $( "#places" ).show();
                } else {
                    $("#places_btn").removeClass("active");
                    $("#routes_btn").addClass("active");

                    $( "#places" ).hide();
                    $( "#routes" ).show();
                }
            }).trigger('hashchange');
        });
    </script>
@endsection
