@extends('layouts.app')

@section('title', 'Oblíbené')

@section('head')
    <style>
        #places {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <ul class="nav nav-tabs bg-primary w-100">
            <li class="nav-item mx-auto">
                <a class="nav-link active" href="#routes" id="routes_btn">TRASY</a>
            </li>
            <li class="nav-item mx-auto">
                <a class="nav-link" href="#places" id="places_btn">MÍSTA</a>
            </li>
        </ul>
    </div>

    <div class="row" id="routes">
        <div class="container">
            <div class="col-auto mt-3">
                <span>Oblíbených tras: {{$routes_count}}</span>
            </div>

            <div class="list-group">
                @foreach($routes as $route)
                    <a href="{{route('view_route', $route->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($route->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($route->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($route->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-chart-line"></i>
                                        {{$route->difficulty}}
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-route"></i>
                                        cca {{$route->distance}} km
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-stopwatch"></i>
                                        @if((floor($route->estimated_time/60)))
                                            @if ((floor($route->estimated_time/60)) > 0)
                                                {{(floor($route->estimated_time/60))}} hod.
                                            @endif
                                            @if (($route->estimated_time%60) > 0)
                                                {{($route->estimated_time%60)}} min.
                                            @endif
                                        @else
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    </span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row" id="places">
        <div class="container">
            <div class="col-auto mt-3">
                <span>Oblíbených míst: {{$places_count}}</span>
            </div>

            <div class="list-group">
                @foreach($places as $place)
                    <a href="{{route('view_place', $place->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($place->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($place->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($place->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            @if ($place->address)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-map-marker-alt"></i>
                                        {{ \Illuminate\Support\Str::limit($place->address, 25, $end='...') }}
                                    </span>
                                </div>
                            @endif

                            @if ($place->entrance_fee)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="far fa-money-bill-alt"></i>
                                        {{$place->entrance_fee}}
                                    </span>
                                </div>
                            @endif
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            $(window).on('hashchange', function(){
                var hash = location.hash;

                if (hash === '#places') {
                    $("#routes_btn").removeClass("active");
                    $("#places_btn").addClass("active");

                    $( "#routes" ).hide();
                    $( "#places" ).show();
                } else {
                    $("#places_btn").removeClass("active");
                    $("#routes_btn").addClass("active");

                    $( "#places" ).hide();
                    $( "#routes" ).show();
                }
            }).trigger('hashchange');
        });
    </script>
@endsection
