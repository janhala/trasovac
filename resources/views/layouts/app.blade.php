<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') · Trasovač</title>

    <meta name="description" content="Trasovač - aplikace pro sdílení turisticky zajímavých míst a tras">
    <meta name="keywords" content="turistika, místa, trasy, aplikace, sdílení, komunita">
    <meta name="author" content="Jan Hála">
    <link rel="icon" href="{{ asset('img/favicon.ico') }}" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('img/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('img/favicon-16x16.png') }}">
    <link rel="mask-icon" href="{{ asset('img/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#00aba9">
    <meta name="theme-color" content="#ffffff">

    @laravelPWA

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- FontAwesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />

    <!-- Styles -->
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">

    @livewireStyles

    <!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">

    <!-- Material Design for Bootstrap CSS -->
    <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous">

    <!-- Snackbar -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/node-snackbar/0.1.16/snackbar.min.css" integrity="sha512-pKdIPri9vb1aqcci9Rsl1xO7n74wHe5+RMcqRufN36EnXVtOsbct0VKDtBdhAnSCqZeny2jiZBSpXp0iDSzZ+Q==" crossorigin="anonymous" />

    <style>
        .list-group-item :first-child {
            margin-right: 5px;
        }

        .row {
            margin: 0;
        }

        .add-route-btn {
            position: fixed;
            right: 5%;
            bottom: 5%;
            z-index: 999;
        }

        html, body {
            height: 100%;
        }

        .modal-backdrop {
            display: none;
        }

        header {
            z-index: auto !important;
        }

        .bmd-list-group-col {
            width: 80%;
        }

        .list-group-item-text {
            line-height: 1.3;
        }

        .list-group-item-heading {
            word-wrap: break-word;
            display: flex;
        }

        .list-group-item-heading span {
            word-wrap: break-word;
        }

        .reviews .list-group-item :first-child, .reviews .list-group-item .material-icons~.material-icons:last-child {
            margin-right: 0 !important;
            padding-left: 0 !important;
        }

        .progress {
            height: 15px;
        }

        .point_lat-lng-wrapper {
            display: none;
        }

        .map-buttons {
            position: relative;
            top: -200px;
            width: 100%;
            height: 0px;
        }

        [class*=" bmd-label"], [class^=bmd-label] {
            color: rgba(0, 0, 0, 0.95);
        }

        .checkbox-inline, .checkbox label, .is-focused .checkbox-inline, .is-focused .checkbox label, .is-focused .radio-inline, .is-focused .radio label, .is-focused .switch label, .radio-inline, .radio label, .switch label {
            color: rgba(0, 0, 0, 0.70);
        }

        .checkbox-inline:active, .checkbox-inline:focus, .checkbox-inline:hover, .checkbox label:active, .checkbox label:focus, .checkbox label:hover, .is-focused .checkbox-inline:active, .is-focused .checkbox-inline:focus, .is-focused .checkbox-inline:hover, .is-focused .checkbox label:active, .is-focused .checkbox label:focus, .is-focused .checkbox label:hover, .is-focused .radio-inline:active, .is-focused .radio-inline:focus, .is-focused .radio-inline:hover, .is-focused .radio label:active, .is-focused .radio label:focus, .is-focused .radio label:hover, .is-focused .switch label:active, .is-focused .switch label:focus, .is-focused .switch label:hover, .radio-inline:active, .radio-inline:focus, .radio-inline:hover, .radio label:active, .radio label:focus, .radio label:hover, .switch label:active, .switch label:focus, .switch label:hover {
            color: rgba(0, 0, 0, 0.95);
        }

        .bmd-help, .text-muted {
            color: #585b5f!important;
        }
    </style>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>

    @yield('head')
</head>

<body>

<div class="bmd-layout-container bmd-drawer-f-l bmd-drawer-overlay">
    <header class="bmd-layout-header">
        <div class="navbar navbar-light bg-faded">
            <button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-p1">
                <span class="sr-only">Toggle drawer</span>
                <i class="material-icons">menu</i>
            </button>
            <ul class="nav navbar-nav">
                <li class="nav-item"><span class="navbar-brand">@yield('title')</span></li>
            </ul>
        </div>
    </header>
    <div id="dw-p1" class="bmd-layout-drawer bg-faded">
        <header>
            @auth
                <a class="navbar-brand">{{ Auth::user()->first_name }} {{ Auth::user()->surname }}</a>
            @endauth

            @guest
                    <a class="navbar-brand" href="{{ route('login') }}">Přihlásit se</a>
            @endguest
        </header>

        <ul class="list-group navbar-nav">
            <li class="nav-item">
                <a class="list-group-item" href="{{ route('browse_routes') }}">Procházet</a>
            </li>
            @guest
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('about_app') }}">O aplikaci</a>
                </li>
            @endguest
            @auth
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('my_routes') }}">Tvorba tras</a>
                </li>
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('imported_routes') }}">Import tras</a>
                </li>
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('my_places') }}">Tvorba míst</a>
                </li>
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('favorite_places') }}">Oblíbené</a>
                </li>
            @endauth
        </ul>

        @auth
            <ul class="list-group navbar-nav align-text-bottom position-absolute bottom-0">
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('about_app') }}">O aplikaci</a>
                </li>
                <li class="nav-item">
                    <a class="list-group-item" href="{{ route('profile_settings') }}">Nastavení profilu</a>
                </li>
                <li class="nav-item">
                    <form method="POST" action="{{ route('logout') }}">
                        @csrf

                        <button type="submit" class="list-group-item" style="color: #009688;">
                            {{ __('Odhlásit se') }}
                        </button>
                    </form>
                </li>
            </ul>
        @endauth
    </div>
    <main class="bmd-layout-content">
        @yield('content')
    </main>
</div>

<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

<!-- Snackbar -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/node-snackbar/0.1.16/snackbar.min.js" integrity="sha512-iILlngu0qmiyIkOH6MV1RWSya+DL2uzo0cb/nKR4hqwz9H+Xnop1++f8TMw1j5CdbutXGkBUyfRUfg/hmNBfZg==" crossorigin="anonymous"></script>

<!-- Bootstrap core JavaScript -->
<script src="https://unpkg.com/popper.js@1.12.6/dist/umd/popper.js" integrity="sha384-fA23ZRQ3G/J53mElWqVJEGJzU0sTs+SvzG8fXVWP+kJQ1lwFAOkcUOysnlKJC33U" crossorigin="anonymous"></script>
<script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>

@yield('scripts')

@livewireScripts
</body>
</html>
