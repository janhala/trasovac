@extends('layouts.app')

@section('title', 'Přihlásit se')

@section('content')
    <div class="row">
        <div class="container">
            <x-jet-authentication-card>
                <x-jet-validation-errors class="mb-4" />

                @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ session('status') }}
                    </div>
                @endif

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group">
                        <x-jet-label for="email" value="{{ __('Email') }}" class="bmd-label-static" />
                        <x-jet-input id="email" class="block mt-1 w-full form-control" type="text" name="email" :value="old('email')" required autofocus />
                    </div>

                    <div class="mt-4 form-group">
                        <x-jet-label for="password" value="{{ __('Heslo') }}" class="bmd-label-static" />
                        <x-jet-input id="password" class="block mt-1 w-full form-control" type="password" name="password" required autocomplete="current-password" />
                    </div>

                    <div class="block mt-4 checkbox">
                        <label for="remember_me" class="flex items-center">
                            <x-jet-checkbox id="remember_me" name="remember" />
                            <span class="ml-2 text-sm text-gray-600">{{ __('Pamatovat si mě') }}</span>
                        </label>
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        @if (Route::has('password.request'))
                            <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                                {{ __('Zapomněli jste své heslo?') }}
                            </a>
                        @endif

                        <x-jet-button class="ml-4 btn btn-primary btn-raised">
                            {{ __('Přihlásit se') }}
                        </x-jet-button>
                    </div>

                    <div class="flex items-center mt-5">
                        <a class="mx-auto text-center underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('register') }}">
                            {{ __('Pokud ještě nemáte vytvořený účet, zaregistrujte se.') }}
                        </a>
                    </div>
                </form>
            </x-jet-authentication-card>
        </div>
    </div>
@endsection
