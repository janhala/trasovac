@extends('layouts.app')

@section('title', 'Zapomenuté heslo')

@section('content')
    <div class="row">
        <div class="container">
            <x-jet-authentication-card>
                <x-slot name="logo">
                    <x-jet-authentication-card-logo />
                </x-slot>

                <div class="mb-4 text-sm text-gray-600">
                    {{ __('Zapomněli jste heslo? Žádný problém. Stačí, když nám sdělíte svou e-mailovou adresu a my vám pošleme e-mailem odkaz na obnovení hesla, který vám umožní vybrat si nové heslo.') }}
                </div>

                @if (session('status'))
                    <div class="mb-4 font-medium text-sm text-green-600">
                        {{ session('status') }}
                    </div>
                @endif

                <x-jet-validation-errors class="mb-4" />

                <form method="POST" action="{{ route('password.email') }}">
                    @csrf

                    <div class="block">
                        <x-jet-label for="email" value="{{ __('Email') }}" class="bmd-label-static" />
                        <x-jet-input id="email" class="block mt-1 w-full form-control" type="email" name="email" :value="old('email')" required autofocus />
                    </div>

                    <div class="flex items-center justify-end mt-4">
                        <x-jet-button class="ml-4 btn btn-primary btn-raised">
                            {{ __('Zaslat odkaz k obnovení') }}
                        </x-jet-button>
                    </div>
                </form>
            </x-jet-authentication-card>
        </div>
    </div>
@endsection
