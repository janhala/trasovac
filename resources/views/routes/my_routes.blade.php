@extends('layouts.app')

@section('title', 'Tvorba tras')

@section('content')
    <div class="row">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mt-3" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div class="col-auto mt-3">
                <span>Vytvořeno tras: {{$routes_count}}</span>
            </div>

            <div class="list-group">
                @foreach($routes as $route)
                    <a href="{{route('view_route', $route->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($route->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($route->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($route->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-chart-line"></i>
                                        {{$route->difficulty}}
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-route"></i>
                                        cca {{$route->distance}} km
                                    </span>
                            </div>

                            <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-stopwatch"></i>
                                        @if((floor($route->estimated_time/60)))
                                            @if ((floor($route->estimated_time/60)) > 0)
                                                {{(floor($route->estimated_time/60))}} hod.
                                            @endif
                                            @if (($route->estimated_time%60) > 0)
                                                {{($route->estimated_time%60)}} min.
                                            @endif
                                        @else
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    </span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <button type="button" class="btn btn-success bmd-btn-fab add-route-btn">
                <i class="material-icons">add</i>
            </button>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $( ".add-route-btn" ).click(function() {
                window.location.href = "{{ route('create_route')}}";
            });
        });
    </script>
@endsection
