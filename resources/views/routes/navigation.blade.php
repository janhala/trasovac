@extends('layouts.app')

@section('title', 'Navigace po trase')

@section('head')
    <style>
        .lat-lng-wrapper {
            display: none;
        }

        #geolocation_alert_wrapper {
            display: none;
        }

        #description_wrapper {
            display: none;
        }

        #content_image {
            max-height: 250px;
        }

        .content-image-wrapper {
            display: none;
        }

        .content-text-wrapper {
            display: none;
        }

        .content-sound-wrapper {
            display: none;
        }

        #show_quiz_btn_wrapper {
            display: none;
        }

        #quiz_wrapper {
            display: none;
            max-width: 400px;
        }

        #show_content_btn_wrapper {
            display: none;
        }

        #next_point_btn_wrapper {
            display: none;
        }

        #contents_wrapper {
            display: none;
        }

        #congrats_btn_wrapper {
            display: none;
        }
    </style>

    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
@endsection

@section('content')
    <div class="row">
        <div class="container p-0">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <div id="map" style="position: relative; overflow: hidden; height: 210px;"></div>
            <div class="row map-buttons">
                <div class="pl-3">
                    <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="exit_btn">
                        <i class="material-icons">exit_to_app</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="container pt-3 pl-3 pr-3">
            <div class="row">
                <div class="col-sm w-50">
                    <div class="row">
                        <p id="instructions">Pokračujte na</p>
                    </div>
                    <div class="row">
                        <h4>
                            {{ \Illuminate\Support\Str::limit($point->name, 30, $end='...') }}
                        </h4>
                    </div>
                </div>
                <div class="col-sm w-50" id="navigate_btn_wrapper">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="navigate_btn">
                            <i class="material-icons">navigation</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto text-center">
                            NAVIGOVAT
                        </p>
                    </div>
                </div>
                <div class="col-sm w-50" id="show_quiz_btn_wrapper">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="show_quiz_btn">
                            <i class="material-icons">loop</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto text-center">
                            ZOBRAZIT KVÍZ
                        </p>
                    </div>
                </div>
                <div class="col-sm w-50" id="show_content_btn_wrapper">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="show_content_btn">
                            <i class="material-icons">loop</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto text-center">
                            ZOBRAZIT OBSAH
                        </p>
                    </div>
                </div>
                <div class="col-sm w-50" id="next_point_btn_wrapper">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="next_point_btn">
                            <i class="material-icons">arrow_forward_ios</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto text-center">
                            POKRAČOVAT V TRASE
                        </p>
                    </div>
                </div>
                <div class="col-sm w-50" id="congrats_btn_wrapper">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="cg_btn">
                            <i class="material-icons">emoji_events</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto text-center">
                            DORAZIL JSTE DO CÍLE!
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="row">
                @if (isset($point->address))
                    <div class="mx-auto go-on-address">
                        <div class="row">
                            <span class="mx-auto">
                                <b>
                                    Pokračujte na
                                    @if ($point->address)
                                        adresu
                                    @else
                                        souřadnice
                                    @endif
                                </b>
                            </span>
                        </div>
                        <div class="row">
                            <p class="mx-auto">
                                @if ($point->address)
                                    {{ \Illuminate\Support\Str::limit($point->address, 70, $end='...') }}
                                @else
                                    {{$point->lat}}, {{$point->lng}}
                                @endif
                            </p>
                        </div>
                    </div>
                @else
                    <div class="mx-auto go-on-coords">
                        <div class="row">
                            <span class="mx-auto"><b>Pokračujte na souřadnice</b></span>
                        </div>
                        <div class="row">
                            <p class="mx-auto">{{$point->lat}}, {{$point->lng}}</p>
                        </div>
                    </div>
                @endif
                @if (isset($point->entrance_fee))
                    <div class="mx-auto">
                        <div class="row">
                            <span class="mx-auto"><b>Vstupné</b></span>
                        </div>
                        <div class="row">
                            <p class="mx-auto">{{$point->entrance_fee}} Kč</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <div class="container pt-2 text-center" id="location_info_wrapper">
            <div class="row">
                <div class="mx-auto">
                    <div class="row">
                        <div class="mx-auto">
                            <p>Zatím jste nedorazil do cílové adresy.</p>
                            <div id="distance_wrapper">
                                <p>Od bodu jste aktuálně vzdálený
                                    <span id="distance"></span>.</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <p class="mx-auto">
                            <span class="material-icons">place</span>
                            Cílové souřadníce: {{$point->lat}}, {{$point->lng}}
                        </p>
                    </div>
                    <div class="row" id="geolocation_alert_wrapper">
                        <div class="alert alert-danger w-100" role="alert">
                            <span id="geolocation_alert"></span>
                        </div>
                    </div>
                    <div id="geolocation_info_wrapper">
                        <div class="row">
                            <p class="mx-auto">
                                <span class="material-icons">my_location</span>
                                Vaše souřadníce: <span id="current_lat_lng"></span></p>
                        </div>
                        <div class="row">
                            <p class="mx-auto">Vaše poloha bude znovu oveřena za <span id="countdown">-</span>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container" id="description_wrapper">
            <div class="row">
                <div class="col">
                    <p class="font-italic" id="description">{{$point->description}}</p>
                </div>
            </div>
        </div>

        <div class="container" id="contents_wrapper">
            <div class="row pt-2">
                <div class="col-sm content-image-wrapper">
                    <img src="" id="content_image" class="img-fluid" alt="Obsah u bodu - obrázek">
                    <p class="font-italic" id="content_image_text"></p>
                </div>
                <div class="col-sm content-text-wrapper">
                    <p class="pt-2 pb-1" id="content_text"></p>
                </div>
                <div class="col-sm content-sound-wrapper">
                    <p class="font-italic mb-0" id="content_sound_text"></p>
                    <audio id="content_sound" controls>
                        Váš prohlížeč nepodporuje vložený zvukový prvek.
                    </audio>
                </div>
            </div>
        </div>

        <div class="container" id="quiz_wrapper">
            <p>Kvíz s názvem: <b><span id="quiz_name"></span></b></p>
            <p class="font-italic" id="quiz_description"></p>
            <div class="row pt-2">
                <p>Otázka zní: <span id="quiz_question"></span></p>
            </div>
            <div class="container">
                <form id="quiz_form">
                    <div id="quiz_answers">
                    </div>
                    <button type="button" id="send_quiz_answers_btn" class="btn btn-primary btn-raised w-100">Odeslat odpovědi</button>
                </form>
            </div>
            <p class="font-italic text-center">Jako správnou odpověď můžete zvolit libovolný počet odpovědí.</p>
            <p class="font-italic text-center">Pro pokračování je třeba tento kvíz vyplnit.</p>
        </div>
    </div>
@endsection

@section('scripts')
    <script
        defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&callback=initMap&libraries=&v=weekly"
    ></script>

    <script>
        function initMap() {
            const lat = {{$point->lat}};
            const lng = {{$point->lng}};
            var placeLocation = { lat: lat, lng: lng };

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 13,
                center: placeLocation,
                disableDefaultUI: true,
                fullscreenControl: true,
                zoomControl: true
            });

            window.map = map;

            var marker = new google.maps.Marker({
                position: placeLocation,
                map: map
            });
        }

        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
                countdown();
            } else {
                $( "#geolocation_alert_wrapper" ).show();
                $( "#geolocation_info_wrapper" ).hide();

                $( "#geolocation_alert" ).html("Tento prohlížeč nepodporuje geolokaci.");
            }
        }

        function showPosition(position) {
            var lat = position.coords.latitude.toFixed(7);
            var lng = position.coords.longitude.toFixed(7);
            window.lat = lat;
            window.lng = lng;

            $( "#current_lat_lng" ).html(lat+', '+lng);

            if (typeof window.map !== 'undefined') {
                var image = '{{ URL::to('/') }}/img/my_location.png';

                var userLocation = { lat: parseFloat(lat), lng: parseFloat(lng) };
                window.userLocationMarker = new google.maps.Marker({
                    position: userLocation,
                    map: window.map,
                    icon: image,
                    zIndex: 999
                });

                const pointLat = {{$point->lat}};
                const pointLng = {{$point->lng}};

                checkDistanceFromPoint(lat, lng, pointLat, pointLng);
            }
        }

        function checkDistanceFromPoint(currentLat, currentLng, pointLat, pointLng) {
            const distanceKm = distance(currentLat, currentLng, pointLat, pointLng, "K").toFixed(2)
            const distanceM = distanceKm * 1000;

            if (distanceKm < 1) {
                $( "#distance" ).html(distanceM + " m");
            } else {
                $( "#distance" ).html(distanceKm + " km");
            }

            // volat, pokud je uživatel k cíli blíže, než je 100m
            if (distanceM < 100) {
                userArrivedToThePoint();
            }
        }

        function userArrivedToThePoint() {
            clearInterval(window.checkLocationInterval);

            $( "#instructions" ).html('Dorazili jste!');
            $( ".go-on-address" ).hide();
            $( ".go-on-coords" ).hide();
            $( "#location_info_wrapper" ).hide();
            $( "#description_wrapper" ).show();

            $.get( "{{ route('get_contents', ['point_id' => $point->id]) }}", function( data ) {
                if (data !== 'empty') {
                    data.forEach(function(content) {
                        if (content.type === 1) {
                            const image = '{{asset('storage')}}/'+content.file;
                            $("#content_image").attr("src", image);
                            $("#content_image_text").html(content.name);
                            $(".content-image-wrapper").show();
                        } else if (content.type === 2) {
                            const text = content.text;
                            $("#content_text").html(text);
                            $(".content-text-wrapper").show();
                        } else if (content.type === 3) {
                            const sound = '{{asset('storage')}}/'+content.file;
                            var soundElem = $("#content_sound");
                            soundElem.attr("src", sound);
                            $("#content_sound_text").html(content.name);
                            $(".content-sound-wrapper").show();
                        }
                    });

                    $( "#contents_wrapper" ).show();
                } else {
                    $( "#contents_wrapper" ).hide();
                }
            });

            $.get( "{{ route('get_quiz', ['point_id' => $point->id]) }}", function( data ) {
                if (data !== 'empty') {
                    $( "#navigate_btn_wrapper" ).hide();
                    $( "#show_quiz_btn_wrapper" ).show();

                    $( "#quiz_name" ).html(data['quiz'].name);
                    $( "#quiz_description" ).html(data['quiz'].description);
                    $( "#quiz_question" ).html(data['quiz'].question);

                    data['answers'].forEach(function(answer) {
                        $( '<div class="row"> <div class="checkbox"><label><input type="checkbox" value="'+answer.id+'"><span class="checkbox-decorator"><span class="check"></span></span> '+answer.answer+'</label></div> </div>' ).appendTo( $( "#quiz_answers" ) );
                    });
                } else {
                    @if(isset(Auth::user()->id))
                        $.post( "{{ route('arrived_to_point', ['user_id' => Auth::user()->id, 'point_id' => $point->id]) }}", {})
                    @else
                        $.post( "{{ route('arrived_to_point', ['user_id' => 0, 'point_id' => $point->id]) }}", {})
                    @endif
                            .done(function( message ) {
                                if (message === 'point saved') {
                                    $( "#navigate_btn_wrapper" ).hide();
                                    $( "#show_quiz_btn_wrapper" ).hide();
                                    $( "#show_content_btn_wrapper" ).hide();
                                    $( "#next_point_btn_wrapper" ).show();
                                } else if (message === 'end of route') {
                                    $( "#navigate_btn_wrapper" ).hide();
                                    $( "#show_quiz_btn_wrapper" ).hide();
                                    $( "#show_content_btn_wrapper" ).hide();
                                    $( "#next_point_btn_wrapper" ).hide();
                                    $( "#congrats_btn_wrapper" ).show();
                                } else {
                                    localStorage.setItem('current_point', message);

                                    $( "#navigate_btn_wrapper" ).hide();
                                    $( "#show_quiz_btn_wrapper" ).hide();
                                    $( "#show_content_btn_wrapper" ).hide();
                                    $( "#next_point_btn_wrapper" ).show();
                                }
                            });
                    }
            });
        }

        function removeUserLocationMarker() {
            window.userLocationMarker.setMap(null);
        }

        function showError(error) {
            $( "#geolocation_alert_wrapper" ).show();
            $( "#geolocation_info_wrapper" ).hide();
            $( "#distance_wrapper" ).hide();
            var alertElem = $( "#geolocation_alert" );

            switch (error.code) {
                case error.PERMISSION_DENIED:
                    alertElem.html("Sdílení vaší polohy je blokováno. Povolte ho prosím, jinak nebudete moci pokračovat.");
                    break;
                case error.POSITION_UNAVAILABLE:
                    alertElem.html("Informace o poloze nejsou k dispozici.");
                    break;
                case error.TIMEOUT:
                    alertElem.html("Vypršel časový limit požadavku na získání polohy.");
                    break;
                case error.UNKNOWN_ERROR:
                    alertElem.html("Nastala neznámá chyba.");
                    break;
            }
        }

        function countdown() {
            var countDownDate = new Date();
            var currentDate = new Date();
            countDownDate.setTime(currentDate.getTime() + (15 * 1000));
            var x = setInterval(function() {
                var now = new Date().getTime();
                var distance = countDownDate - now;
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                document.getElementById("countdown").innerHTML = seconds + "s";
                if (seconds < 1) {
                    clearInterval(x);
                }
            }, 1000);
        }

        function distance(lat1, lon1, lat2, lon2, unit) {
            if ((lat1 === lat2) && (lon1 === lon2)) {
                return 0;
            }
            else {
                var radlat1 = Math.PI * lat1/180;
                var radlat2 = Math.PI * lat2/180;
                var theta = lon1-lon2;
                var radtheta = Math.PI * theta/180;
                var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
                if (dist > 1) {
                    dist = 1;
                }
                dist = Math.acos(dist);
                dist = dist * 180/Math.PI;
                dist = dist * 60 * 1.1515;
                if (unit==="K") { dist = dist * 1.609344 }
                if (unit==="N") { dist = dist * 0.8684 }
                return dist;
            }
        }

        $( document ).ready(function() {
            @if(!isset(Auth::user()->id))
                if (localStorage.getItem('current_point') !== null) {
                    var pointId = localStorage.getItem('current_point');
                    var url = window.location.href;

                    var actualPointId = url.substring(url.lastIndexOf('/') + 1);
                    if (actualPointId !== pointId) {
                        url = url.replace(/\/[^\/]*$/, '/' + pointId);
                        window.location.href = url;
                    }
                }

            @endif

            getLocation();

            $( "#navigate_btn" ).click(function() {
                window.open('http://maps.google.com/maps?saddr='+window.lat+','+window.lng+'&daddr={{$point->lat}},{{$point->lng}}&dirflg=w');
            });

            window.checkLocationInterval = setInterval(function(){
                removeUserLocationMarker();
                getLocation();
            }, 15000);

            $( "#exit_btn" ).click(function() {
                window.history.back();
            });

            $( "#show_quiz_btn" ).click(function() {
                $( "#contents_wrapper" ).hide();
                $( "#quiz_wrapper" ).show();

                $( "#show_quiz_btn_wrapper" ).hide();
                $( "#show_content_btn_wrapper" ).show();
            });

            $( "#show_content_btn" ).click(function() {
                $( "#contents_wrapper" ).show();
                $( "#quiz_wrapper" ).hide();

                $( "#show_quiz_btn_wrapper" ).show();
                $( "#show_content_btn_wrapper" ).hide();
            });

            $( "#next_point_btn" ).click(function() {
                @if(isset(Auth::user()->id))
                    location.reload();
                @else
                    var pointId = localStorage.getItem('current_point');
                    var url = window.location.href;
                    url = url.replace(/\/[^\/]*$/, '/' + pointId);

                    window.location.href = url;
                @endif

            });

            $( "#congrats_btn_wrapper" ).click(function() {
                @if(isset(Auth::user()->id))
                    location.reload();
                @else
                    localStorage.removeItem('current_point');
                    url = "{{ route('view_route', ['id' => $point->route->id]) }}";
                    window.location.href = url;
                @endif

            });

            $( "#send_quiz_answers_btn" ).click(function() {
                $( "#send_quiz_answers_btn" ).prop('disabled', true);

                let selected_answers = [];

                $('#quiz_form input[type=checkbox]').each(function () {
                    if (this.checked) {
                        var id = $(this).val();
                        selected_answers.push(id);
                    }
                });

                @if(isset(Auth::user()->id))
                    $.post( "{{ route('check_quiz_answers', ['user_id' => Auth::user()->id, 'point_id' => $point->id]) }}", { selected_answers:  selected_answers})
                @else
                    $.post( "{{ route('check_quiz_answers', ['user_id' => 0, 'point_id' => $point->id]) }}", { selected_answers:  selected_answers})
                @endif
                    .done(function( data ) {
                        if (data.message === 'no answers selected') {
                            Snackbar.show({
                                text: 'Nebyly vybrány žádné odpovědi.',
                                actionText: 'OK',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center'
                            });

                            $( "#send_quiz_answers_btn" ).prop('disabled', false);
                        } else if (data.message === 'completed correctly') {
                            @if(!isset(Auth::user()->id))
                                if(data.data.next_point_id) {
                                    localStorage.setItem('current_point', data.data.next_point_id);
                                }
                            @endif

                            Snackbar.show({
                                text: 'Gratulujeme!!! Kvíz jste dokončil úspěšně, nyní tedy můžete pokračovat na další bod trasy, stačí kliknout na "Pokračovat v trase".',
                                actionText: 'OK',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center'
                            });


                            $( "#show_quiz_btn_wrapper" ).hide();
                            $( "#show_content_btn_wrapper" ).hide();
                            $( "#next_point_btn_wrapper" ).show();
                        } else if (data.message === 'end of route') {
                            Snackbar.show({
                                text: 'Gratulujeme!!! Kvíz jste dokončil úspěšně, jste v cíli!',
                                actionText: 'OK',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center'
                            });


                            $( "#show_quiz_btn_wrapper" ).hide();
                            $( "#show_content_btn_wrapper" ).hide();
                            $( "#next_point_btn_wrapper" ).hide();
                            $( "#congrats_btn_wrapper" ).show();
                        } else if (data.message === 'repeat quiz') {
                            Snackbar.show({
                                text: 'Kvíz byl vyplněn chybně, můžete ho ale opakovat.',
                                actionText: 'OK',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center'
                            });

                            $( "#send_quiz_answers_btn" ).prop('disabled', false);
                        } else if (data.message === 'go to the point') {
                            Snackbar.show({
                                text: 'Kvíz byl vyplněn chybně. Navštivte bod s nápovědou, poté znovu načtěte tuto stránku a kvíz opakujte. Nápověda: ' + data.data.hint,
                                actionText: 'Zobrazit bod s nápovědou',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center',
                                duration: 10000,
                                onClose: function(element) {
                                    window.open('http://maps.google.com/maps?saddr='+window.lat+','+window.lng+'&daddr='+data.data.lat+','+data.data.lng+'&dirflg=w');
                                }
                            });
                        } else if (data.message === 'show hint') {
                            Snackbar.show({
                                text: 'Kvíz byl vyplněn chybně, můžete ho ale opakovat. Nápověda: ' + data.data.hint,
                                actionText: 'OK',
                                actionTextColor: '#4CAF50',
                                pos: 'bottom-center'
                            });

                            $( "#send_quiz_answers_btn" ).prop('disabled', false);
                        }
                    });
            });
        });
    </script>
@endsection
