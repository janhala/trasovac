@extends('layouts.app')

@section('title', 'Zobrazit trasu')

@section('head')
    <style>
        .lat-lng-wrapper {
            display: none;
        }

        .list-group-item .list-group-item-text {
            max-height: 100%;
        }

        @if($favorite > 0)
            #add_to_favorites_btn {
            display: none;
        }
        @else
            #remove_from_favorites_btn {
            display: none;
        }
        @endif
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="container p-0">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mb-0" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div id="map" style="position: relative; overflow: hidden; height: 210px;"></div>
            <div class="row map-buttons">
                <div class="col-sm w-50">
                    <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="go_back_btn">
                        <i class="material-icons">arrow_back</i>
                    </button>
                </div>
                <div class="col-sm w-50 text-right">
                    @if (isset(Auth::user()->id) && Auth::user()->id !== $route->user_id)
                        <div class="col p-0">
                            <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="add_to_favorites_btn">
                                <i class="material-icons">favorite_border</i>
                            </button>
                            <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="remove_from_favorites_btn">
                                <i class="material-icons">favorite</i>
                            </button>
                        </div>
                    @endif
                    <div class="col p-0">
                        @if ($route->public !== 0 || isset($route->url_for_teamwork) || isset($route->url) || isset($route->numeric_code))
                            <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="share_button" data-toggle="modal" data-target="#CopyURLModal">
                                <i class="material-icons">share</i>
                            </button>
                        @endif

                        <div class="modal fade pt-5" id="CopyURLModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        @if (!isset(Auth::user()->id) || (isset(Auth::user()->id) && Auth::user()->id !== $route->user_id) || ($route->url === null && $route->url_for_teamwork === null && $route->numeric_code === null))
                                            <h5 class="modal-title" id="exampleModalLabel">Sdílet trasu</h5>
                                        @else
                                            <h5 class="modal-title" id="exampleModalLabel">Sdílení a spolupráce</h5>
                                        @endif
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        @if (!isset(Auth::user()->id) || (isset(Auth::user()->id) && Auth::user()->id !== $route->user_id) || ($route->url === null && $route->numeric_code === null))
                                            @if ($route->public === 1)
                                                <div class="container pb-3">
                                                    <div class="row">
                                                        <div class="col">
                                                            <div class="row">
                                                                <button type="button" class="btn btn-success bmd-btn-fab mx-auto">
                                                                    <i class="material-icons" id="share_on_facebook_btn">facebook</i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col">
                                                            <div class="row">
                                                                <button type="button" class="btn btn-success bmd-btn-fab mx-auto">
                                                                    <i class="material-icons" id="share_by_email_btn">mail</i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <form class="m-0">
                                                    <input type="text" class="form-control" id="input_with_url" name="input_with_url" />
                                                </form>
                                                <button type="button" class="btn btn-primary" onclick="copyToClipboard();">Zkopírovat URL trasy</button>
                                            @endif
                                            @if ($route->url_for_teamwork !== null)
                                                <div class="row pt-2">
                                                    <div class="container">
                                                        <p class="float-left font-weight-bold">Odkaz pro spolupráci:</p>
                                                        <form class="m-0">
                                                            <input type="text" class="form-control" id="input_with_teamwork_url" name="input_with_teamwork_url" value="{{ route('teamwork', [$route->url_for_teamwork, $route->id]) }}" />
                                                        </form>
                                                        <button type="button" class="btn btn-primary float-right mt-1 mb-1" onclick="copyToClipboardTeamworkUrl();">Zkopírovat URL pro spolupráci</button>
                                                    </div>
                                                </div>
                                                <hr />
                                            @endif
                                        @else
                                            @if ($route->url !== null)
                                                <div class="row pb-2">
                                                    <div class="container">

                                                        <p class="float-left font-weight-bold">Odkaz pro sdílení:</p>
                                                        <form class="m-0">
                                                            <input type="text" class="form-control" id="input_with_private_url" name="input_with_private_url" value="{{ route('import_route', [$route->url, $route->id]) }}" />
                                                        </form>
                                                        <button type="button" class="btn btn-primary float-right mt-1 mb-1" onclick="copyToClipboardPrivateUrl();">Zkopírovat URL pro sdílení</button>
                                                    </div>
                                                </div>
                                                <hr />
                                            @endif
                                            @if ($route->numeric_code !== null)
                                                <div class="row pt-2">
                                                    <div class="container">
                                                        <p class="float-left text-center">Číselný kód pro sdílení trasy je <span class="font-weight-bold">{{$route->numeric_code}}</span></p>
                                                    </div>
                                                </div>
                                                <hr />
                                            @endif
                                            @if ($route->url_for_teamwork !== null)
                                                <div class="row pt-2">
                                                    <div class="container">
                                                        <p class="float-left font-weight-bold">Odkaz pro spolupráci:</p>
                                                        <form class="m-0">
                                                            <input type="text" class="form-control" id="input_with_teamwork_url" name="input_with_teamwork_url" value="{{ route('teamwork', [$route->url_for_teamwork, $route->id]) }}" />
                                                        </form>
                                                        <button type="button" class="btn btn-primary float-right mt-1 mb-1" onclick="copyToClipboardTeamworkUrl();">Zkopírovat URL pro spolupráci</button>
                                                    </div>
                                                </div>
                                                <hr />
                                            @endif
                                        @endif
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                                        @if (!isset(Auth::user()->id) || (isset(Auth::user()->id) && Auth::user()->id !== $route->user_id) || ($route->url === null && $route->url_for_teamwork === null  && $route->numeric_code === null))
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pt-3 pl-3 pr-3">
            <div class="row">
                <div class="col-sm w-50">
                    <div class="row">
                        <h4>
                            {{ \Illuminate\Support\Str::limit($route->name, 35, $end='...') }}
                        </h4>
                    </div>
                    <div class="row">
                        <p>{{$points->count()}} body trasy</p>
                    </div>
                </div>
                <div class="col-sm w-50">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="navigate_btn">
                            <i class="material-icons">hiking</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto">
                            PROJÍT
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="mx-auto">
                            <div class="row">
                                <span class="mx-auto"><b>Obtížnost</b></span>
                            </div>
                            <div class="row">
                                <p class="mx-auto">{{$route->difficulty}}</p>
                            </div>
                        </div>
                        <div class="mx-auto">
                            <div class="row">
                                <span class="mx-auto"><b>Délka trasy</b></span>
                            </div>
                            <div class="row">
                                <p class="mx-auto">cca {{$route->distance}} km</p>
                            </div>
                        </div>
                        <div class="mx-auto">
                            <div class="row">
                                <span class="mx-auto"><b>Čas potřebný k projití</b></span>
                            </div>
                            <div class="row">
                                <p class="mx-auto">
                                    @if((floor($route->estimated_time/60)))
                                        @if ((floor($route->estimated_time/60)) > 0)
                                            {{(floor($route->estimated_time/60))}} hod.
                                        @endif
                                        @if (($route->estimated_time%60) > 0)
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    @else
                                        {{($route->estimated_time%60)}} min.
                                    @endif
                                </p>
                            </div>
                        </div>
                        @if (!isset(Auth::user()->id) || isset(Auth::user()->id) && Auth::user()->id !== $route->user_id)
                            <div class="mx-auto">
                                <div class="row">
                                    <span class="mx-auto"><b>Autorem trasy je</b></span>
                                </div>
                                <div class="row">
                                    <p class="mx-auto">{{$route->author->first_name}} {{$route->author->surname}}</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="container pb-3">
                    <div class="row">
                        @if ((isset(Auth::user()->id) && $route->user_id === Auth::user()->id) || $existing_teamwork === 1)
                        <div class="col text-center">
                            <form method="POST" action="{{ route('update_route', ['id' => $route->id]) }}">
                                @csrf
                                <button type="submit" class="btn btn-raised btn-success">
                                    <i class="fas fa-pencil-alt"></i> Upravit
                                </button>
                            </form>
                        </div>
                        @endif
                        @if (isset(Auth::user()->id) && $route->user_id === Auth::user()->id)
                            <div class="col text-center">
                                <form method="POST" action="{{ route('delete_route', ['id' => $route->id]) }}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-raised btn-danger">
                                        <i class="fas fa-trash"></i> Smazat
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="container">
                    <p>{{$route->description}}</p>
                </div>

                <div class="container">
                    <p class="mb-0"><b>Body na trase:</b></p>

                    <ul class="list-group">
                        @foreach($points as $key=>$point)
                            <li>
                                <a class="list-group-item">
                                    <i class="material-icons">place</i>
                                    <div class="bmd-list-group-col">
                                        <p class="list-group-item-heading">
                                            {{$alphabet[$key]}}) {{ \Illuminate\Support\Str::limit($point->name, 70, $end='...') }}
                                        </p>
                                        <p class="list-group-item-text">
                                            @if (isset($point->address) && $point->address !== "")
                                                {{ \Illuminate\Support\Str::limit($point->address, 70, $end='...') }}
                                            @else
                                                {{$point->lat}},{{$point->lng}}
                                            @endif
                                        </p>
                                        @if (isset($point->entrance_fee))
                                            <p class="list-group-item-text">Vstupné: {{$point->entrance_fee}} Kč</p>
                                        @endif
                                    </div>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </div>

                @if ($items->isNotEmpty())
                    <div class="container">
                        <p class="mb-0"><b>Předměty s sebou:</b></p>

                        <ul class="list-group">
                            @foreach($items as $item)
                                <li>
                                    <a class="list-group-item">
                                        <i class="material-icons">backpack</i>
                                        <div class="bmd-list-group-col">
                                            <p class="list-group-item-heading">
                                                {{ \Illuminate\Support\Str::limit($item->name, 60, $end='...') }}
                                            </p>
                                            <p class="list-group-item-text">
                                                @if ($item->required == 1)
                                                    povinný
                                                @else
                                                    doporučený
                                                @endif
                                            </p>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="container reviews">
                    <p class="mb-0"><b>Recenze trasy:</b></p>

                    @if ($reviews->isNotEmpty())
                        @if (isset(Auth::user()->id) && $review->isNotEmpty())
                            <p class="mt-2 font-italic">Toto místo jste již hodnotil.</p>
                        @elseif (isset(Auth::user()->id) && $already_completed_route->isNotEmpty() && Auth::user()->id !== $route->user_id)
                            <div class="col mt-2">
                                <button type="button" class="btn btn-raised btn-success" data-toggle="modal" data-target="#add_review_modal"><i class="fas fa-star"></i> Napsat recenzi</button>
                            </div>
                        @elseif (isset(Auth::user()->id) && Auth::user()->id === $route->user_id)
                            <p class="mt-2 font-italic">Jako autor trasy tuto trasu nemůžete hodnotit.</p>
                        @elseif (!isset(Auth::user()->id))
                            <p class="mt-2 font-italic">Pro hodnocení trasy je třeba přihlásit se do svého uživatelského účtu a trasu absolvovat.</p>
                        @else
                            <p class="mt-2 font-italic">Trasu budete moci hodnotit po jejím projití.</p>
                        @endif
                        <ul class="list-group">
                            @foreach($reviews as $review)
                                <li>
                                    <a class="list-group-item">
                                        <div class="">
                                            @for($x = 0; $x < $review->number_of_stars; $x++)
                                                <i class="material-icons">star</i>
                                            @endfor
                                        </div>
                                        <div class="bmd-list-group-col pl-2 w-50">
                                            <p class="list-group-item-heading pt-3">{{$review->users->first_name}} {{$review->users->surname}}</p>
                                            <p class="list-group-item-text">
                                                {{$review->comment}}
                                            </p>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        @if (isset(Auth::user()->id) && $review->isNotEmpty())
                            <p class="mt-2 font-italic">Toto místo jste již hodnotil.</p>
                        @elseif (isset(Auth::user()->id) && $already_completed_route->isNotEmpty() && Auth::user()->id !== $route->user_id)
                            <div class="col mt-2">
                                <button type="button" class="btn btn-raised btn-success" data-toggle="modal" data-target="#add_review_modal"><i class="fas fa-star"></i> Napsat recenzi</button>
                            </div>
                        @elseif (isset(Auth::user()->id) && Auth::user()->id === $route->user_id)
                            <p class="mt-2 font-italic">Jako autor trasy tuto trasu nemůžete hodnotit.</p>
                        @elseif (!isset(Auth::user()->id))
                            <p class="mt-2 font-italic">Pro hodnocení trasy je třeba přihlásit se do svého uživatelského účtu a trasu absolvovat.</p>
                        @else
                            <p class="mt-2 font-italic">Trasu budete moci hodnotit po jejím projití.</p>
                        @endif

                        <p class="mt-2">Zatím tu žádná recenze není.</p>
                    @endif
                </div>
            </div>
        </div>

        @if (isset(Auth::user()->id) && $already_completed_route->isNotEmpty())
            <!-- add_review_modal -->
            <div class="modal fade" id="add_review_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <form id="review_form" method="POST" action="{{ route('add_new_review', ['route_id' => $route->id]) }}">
                            @csrf

                            <div class="modal-header">
                                <h5 class="modal-title">Recenze trasy</h5>
                            </div>

                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="number_of_stars" class="bmd-label-static">Počet hvězdiček</label>
                                    <select class="form-control" id="number_of_stars" name="number_of_stars" required>
                                        <option value="1">1 (nejméně)</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5 (nejvíce)</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="comment" class="bmd-label-static">Text recenze</label>
                                    <textarea class="form-control" id="comment" name="comment" rows="3" required></textarea>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                                <button type="submit" class="btn btn-primary">Uložit recenzi</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        @endif
    </div>


@endsection

@section('scripts')
    <script
        defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&callback=initMap&libraries=&v=weekly"
    ></script>

    <script>
        function initMap() {
            const directionsService = new google.maps.DirectionsService();
            const directionsRenderer = new google.maps.DirectionsRenderer();
            const map = new google.maps.Map(document.getElementById("map"), {
                disableDefaultUI: true
            });
            directionsRenderer.setMap(map);

            calculateAndDisplayRoute(directionsService, directionsRenderer);
        }

        function calculateAndDisplayRoute(directionsService, directionsRenderer) {
            const waypts = [];
            var waypointsBetweenArray = JSON.parse(@json($waypoints_between));

            for (let i = 0; i < waypointsBetweenArray.length; i++) {
                if (waypointsBetweenArray) {
                    waypts.push({
                        location: waypointsBetweenArray[i],
                        stopover: true,
                    });
                }
            }

            directionsService.route(
                {
                    origin: "{{$start_point}}",
                    destination: "{{$end_point}}",
                    waypoints: waypts,
                    optimizeWaypoints: true,
                    travelMode: google.maps.TravelMode.WALKING,
                },
                (response, status) => {
                    if (status === "OK" && response) {
                        directionsRenderer.setDirections(response);
                        const route = response.routes[0];
                    } else {
                        window.alert("Nastala chyba při vykreslení trasy.");
                    }
                }
            );
        }

        function copyToClipboard() {
            var input = document.getElementById("input_with_url");
            input.select();
            input.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }

        function copyToClipboardPrivateUrl() {
            var input = document.getElementById("input_with_private_url");
            input.select();
            input.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }

        function copyToClipboardTeamworkUrl() {
            var input = document.getElementById("input_with_teamwork_url");
            input.select();
            input.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }

        $( document ).ready(function() {
            @if ($route->public === 1 && (!isset(Auth::user()->id) || (isset(Auth::user()->id) && Auth::user()->id !== $route->user_id) || ($route->url === null && $route->numeric_code === null)))
                $( "#share_button" ).click(function() {
                    var input = document.getElementById("input_with_url");
                    input.value = window.location.href;
                });
            @endif

            $( "#share_on_facebook_btn" ).click(function() {
                window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href);
            });

            $( "#go_back_btn" ).click(function() {
                window.history.back();
            });

            $( "#navigate_btn" ).click(function() {
                @if(isset(Auth::user()->id))
                    window.location.href = '{{ route('start_navigation', $route->id) }}';
                @else
                    window.location.href = '{{ route('start_nav_without_useracc', [$route->id, 0]) }}';
                @endif

            });

            $( "#share_by_email_btn" ).click(function() {
                Snackbar.show({
                    text: 'Upozornění - sdílení pomocí emailu funguje pouze při použití některých emailových klientů (Gmail, Pošta).',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center',
                    onClose: function() {
                        window.open('mailto:?subject=Objevil jsem novou trasu v aplikaci Trasovač.com&body=Trasa má název "{{ \Illuminate\Support\Str::limit($route->name, 35, $end='...') }}". Podívej se na ní pomocí url: ' + window.location.href);
                    }
                });
            });

            @if(isset(Auth::user()->id))
                $( "#add_to_favorites_btn" ).click(function() {
                    $.post( "{{ route('new_favorite_route') }}", { user_id: {{Auth::user()->id}}, route_id: {{$route->id}} })
                        .done(function( message ) {
                            if (message === 'success') {
                                $( "#add_to_favorites_btn" ).hide();
                                $( "#remove_from_favorites_btn" ).show();
                            }
                        });
                });

                $( "#remove_from_favorites_btn" ).click(function() {
                    $.ajax({
                        method: "DELETE",
                        url: "{{ route('remove_favorite_route') }}",
                        data: { user_id: {{Auth::user()->id}}, route_id: {{$route->id}} }
                    })
                        .done(function( message ) {
                            if (message === 'success') {
                                $( "#remove_from_favorites_btn" ).hide();
                                $( "#add_to_favorites_btn" ).show();
                            }
                        });
                });
            @endif
        });
    </script>
@endsection
