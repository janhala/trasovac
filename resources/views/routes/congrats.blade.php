@extends('layouts.app')

@section('title', 'Jste v cíli!')

@section('content')
    <div class="row">
        <div class="container p-0">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <div class="container">
                <div class="row mt-3">
                    <h3>Gratulujeme!</h3>
                </div>

                <div class="row mt-3">
                    <p class="font-bold">
                        Již jste ušel
                        {{$completed_routes_count}}
                        @if ($completed_routes_count == 1)
                            trasu!
                        @elseif ($completed_routes_count == 2 || $completed_routes_count == 3 || $completed_routes_count == 4)
                            trasy!
                        @else
                            tras!
                        @endif
                    </p>
                </div>

                @if($remaining_routes > 0)
                    <div class="row mt-3">
                        <div class="progress w-100">
                            <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="{{$completed_routes_count}}" aria-valuemin="0" aria-valuemax="{{$routes_count}}" style="width: {{($completed_routes_count/$routes_count)*100}}%"></div>
                        </div>
                    </div>

                    <div class="row font-italic pt-2 text-center">
                        Víte, že v Trasovači můžete projít ještě {{$remaining_routes}}
                        @if($remaining_routes === 1)
                            další trasu?
                        @elseif($remaining_routes === 2 || $remaining_routes === 3 || $remaining_routes === 4)
                            další trasy?
                        @else
                            dalších tras?
                        @endif
                    </div>
                @endif

                <div class="row mt-3">
                    <div class="col text-center">
                        @if(Auth::user()->id === $author->id)
                            <button type="button" class="btn btn-raised btn-success" id="view_route_btn">Zobrazit detail trasy</button>
                        @else
                            <button type="button" class="btn btn-raised btn-success" id="view_route_btn"><i class="fas fa-star"></i> Ohodnotit trasu</button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $( document ).ready(function() {
            $( "#view_route_btn" ).click(function() {
                window.location.href = "{{ route('view_route', ['id' => $route_id])}}";
            });
        });
    </script>
@endsection
