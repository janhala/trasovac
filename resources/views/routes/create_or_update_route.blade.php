@extends('layouts.app')

@section('title', 'Přidat novou trasu')

@section('head')
    <style>
        #content_text_wrapper {
            display: none;
        }

        #content_sound_wrapper {
            display: none;
        }

        #point_with_hint_address_wrapper {
            display: none;
        }

        #quiz_hint_wrapper {
            display: none;
        }

        .quiz_answer_wrapper {
            display: flex;
        }

        #enable_links_wrapper {
            display: none;
        }

        #items_list .list-group-item, #points_list .list-group-item {
            max-width: 75%;
            min-width: 200px;
        }

        #items_list .list-group-item .bmd-list-group-col, #points_list .list-group-item .bmd-list-group-col {
            width: 100% !important;
        }

        #map {
            height: 200px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <form id="route_form" class="pt-2" method="POST" enctype="multipart/form-data" @if (isset($route))action="{{ route('update_route_submit', ['id' => $route->id]) }}" @else action="{{ route('create_route_submit') }}"@endif>
                @if (isset($route))
                    @method('put')
                @endif
                @csrf
                <div class="form-group">
                    <label for="name" class="bmd-label-static">Název</label>
                    <input type="text" class="form-control" name="name" id="name" required @if (isset($route) && $route->name)value="{{$route->name}}"@endif>
                    <small class="text-muted">Zadejte název trasy.</small>
                </div>

                <div class="form-group">
                    <label for="description" class="bmd-label-static">Popis</label>
                    <textarea class="form-control" name="description" id="description" rows="3" required>@if (isset($route) && $route->description){{$route->description}}@endif</textarea>
                    <small class="text-muted">Zadejte popis trasy.</small>
                </div>

                <div class="form-group">
                    <label for="difficulty" class="bmd-label-static">Obtížnost</label>
                    <select class="form-control" id="difficulty" name="difficulty" required>
                        <option value="1" @if (isset($route) && $route->difficulty === 1) selected @endif>1 (nejnižší)</option>
                        <option value="2" @if (isset($route) && $route->difficulty === 2) selected @endif>2</option>
                        <option value="3" @if (isset($route) && $route->difficulty === 3) selected @endif>3</option>
                        <option value="4" @if (isset($route) && $route->difficulty === 4) selected @endif>4</option>
                        <option value="5" @if (isset($route) && $route->difficulty === 5) selected @endif>5 (nejvyšší)</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="estimated_time" class="bmd-label-static">Odhadovaný čas (v min)</label>
                    <input type="number" class="form-control" name="estimated_time" id="estimated_time" min="1" max="10080" required @if (isset($route) && $route->estimated_time)value="{{$route->estimated_time}}"@endif>
                    <small class="text-muted">Zadejte odhadovaný čas (v minutách) potřebný pro projití celé trasy.</small>
                </div>

                <div class="form-group" id="public_wrapper">
                    <div class="font-weight-bold">
                        <p>Přístupnost</p>
                    </div>

                    <div class="radio">
                        <label>
                            <input type="radio" name="public" id="public_1" value="1" @if (isset($route) && $route->public === 1) checked @elseif(!isset($route)) checked @endif>
                            Chci, aby byla trasa přístupná veřejně.
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="public" id="public_0" value="0" @if (isset($route) && $route->public === 0) checked @endif>
                            Chci, aby byla trasa neveřejná.
                        </label>
                    </div>
                </div>

                <div class="font-weight-bold pt-1">
                    <p>Sdílení a spolupráce</p>
                </div>

                <div class="switch">
                    <label>
                        <input type="checkbox" name="request_url_for_teamwork" id="request_url_for_teamwork" @if (isset($route) && $route->url_for_teamwork !== null) checked @endif>
                        Povolit spolupráci na trase a získat odkaz pro sdílení trasy se spolupracovníky.
                    </label>
                </div>

                <div id="enable_links_wrapper">
                    <div class="switch pt-1">
                        <label>
                            <input type="checkbox" name="request_url" id="request_url" @if (isset($route) && $route->url !== null) checked @endif>
                            Získat odkaz pro sdílení trasy.
                        </label>
                    </div>

                    <div class="switch pt-1 pb-3">
                        <label>
                            <input type="checkbox" name="request_numeric_code" id="request_numeric_code" @if (isset($route) && $route->numeric_code !== null) checked @endif>
                            Získat číselný kód pro sdílení trasy.
                        </label>
                    </div>
                </div>

                <hr />

                <div class="font-weight-bold pt-1">
                    <p class="mb-0">Předměty s sebou</p>
                </div>

                <ul class="list-group pb-0" id="items_list">
                    <li>
                        <p class="font-italic">Doporučte uživatelům, co si vzít na cestu s sebou.</p>
                        <p>Seznam předmětů je zatím prázdný.</p>
                        <p>Lze přidat maximálně 10 předmětů.</p>
                    </li>
                </ul>

                <div class="form-group">
                    <label for="item_name" class="bmd-label-static">Název předmětu</label>
                    <input type="text" class="form-control" name="item_name" id="item_name">
                    <small class="text-muted">Zadejte název předmětu. Povinné pole při přidávání předmětu.</small>
                </div>

                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="item_required" id="item_required"> Tento předmět je pro úspěšné dokončení cesty povinný.
                    </label>
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary btn-raised" id="add_item_btn">Uložit předmět</button>
                </div>

                <hr />

                <div class="font-weight-bold pt-1">
                    <p class="mb-0">Body na trase</p>
                </div>

                <ul class="list-group pb-0" id="points_list">
                    <li>
                        <p class="font-italic">Trasu vytvořte pomocí přidání jednotlivých bodů, které se na ní nacházejí.</p>
                        <p>Seznam bodů je zatím prázdný.</p>
                        <p>Lze přidat maximálně 10 bodů.</p>
                    </li>
                </ul>

                <div class="form-group">
                    <label for="point_name" class="bmd-label-static">Název bodu</label>
                    <input type="text" class="form-control" name="point_name" id="point_name">
                    <small class="text-muted">Zadejte název bodu. Povinné pole při přidávání bodu.</small>
                </div>

                <div class="form-group">
                    <label for="point_description" class="bmd-label-static">Popis</label>
                    <textarea class="form-control" name="point_description" id="point_description" rows="3"></textarea>
                    <small class="text-muted">Zadejte popis bodu. Povinné pole při přidávání bodu.</small>
                </div>

                <div class="switch">
                    <label>
                        <input type="checkbox" name="point_address_or_coordination" id="point_address_or_coordination">
                        Namísto adresy zadat souřadnice, či vybrat pozici z mapy.
                    </label>
                </div>

                <div class="form-group" id="point_address_wrapper">
                    <label for="point_address" class="bmd-label-static">Adresa</label>
                    <input type="text" class="form-control" name="point_address" id="point_address" placeholder="">
                    <small class="text-muted">Pomocí nápovědy vyberte adresu bodu. Pro zobrazení nápovědy začněte do pole psát název bodu, či jeho adresu. Povinné pole při přidávání bodu bez zadání souřadnic.</small>
                </div>

                <div class="point_lat-lng-wrapper" id="point_lat_lng_wrapper">
                    <div class="form-group">
                        <label for="point_lat" class="bmd-label-static">Zeměpisná šířka (lat)</label>
                        <input type="number" class="form-control" name="point_lat" id="point_lat">
                        <small class="text-muted">Zadejte zeměpisnou šířku bodu. Povinné pole při přidávání bodu bez adresy.</small>
                    </div>

                    <div class="form-group">
                        <label for="point_lng" class="bmd-label-static">Zeměpisná délka (lng)</label>
                        <input type="number" class="form-control" name="point_lng" id="point_lng">
                        <small class="text-muted">Zadejte zeměpisnou délku bodu. Povinné pole při přidávání bodu bez adresy.</small>
                    </div>

                    <div class="form-group">
                        <label class="bmd-label-static">Výběr pozice z mapy</label>
                        <div id="map"></div>
                        <small class="text-muted">Pokud neznáte příslušné souřadnice, můžete je vybrat přímo z mapy.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="point_entrance_fee" class="bmd-label-static">Vstupné</label>
                    <input type="number" class="form-control" name="point_entrance_fee" id="point_entrance_fee" min="0" max="10000">
                    <small class="text-muted">Zadejte výši vstupného, které je nutné pro navštívení tohoto bodu uhradit, a to v Kč.</small>
                </div>

                <div class="form-group">
                    <button type="button" class="btn btn-primary btn-raised" id="add_point_btn">Uložit bod</button>
                </div>

                <div class="d-none" id="contents_wrapper">
                    @if (isset($contents))
                        @foreach($contents as $content)
                            @if($content->type === 1)
                                <input type="hidden" id="content_image_name_{{$content->point_id}}" name="content_image_name_{{$content->point_id}}" value="{{$content->name}}">
                                <input type="hidden" id="existing_content_image_id_{{$content->point_id}}" name="existing_content_image_id_{{$content->point_id}}" value="{{$content->file}}">
                            @elseif($content->type === 2)
                                <input type="hidden" id="content_text_name_{{$content->point_id}}" name="content_text_name_{{$content->point_id}}" value="{{$content->name}}">
                                <input type="hidden" id="content_text_{{$content->point_id}}" name="content_text_{{$content->point_id}}" value="{{$content->text}}">
                                <input type="hidden" id="existing_content_text_id_{{$content->point_id}}" name="existing_content_text_id_{{$content->point_id}}" value="{{$content->text}}">
                            @elseif($content->type === 3)
                                <input type="hidden" id="content_sound_name_{{$content->point_id}}" name="content_sound_name_{{$content->point_id}}" value="{{$content->name}}">
                                <input type="hidden" id="existing_content_sound_id_{{$content->point_id}}" name="existing_content_sound_id_{{$content->point_id}}" value="{{$content->file}}">
                            @endif
                        @endforeach
                    @endif
                </div>

                <div class="d-none" id="quizzes_wrapper">
                    @if (isset($quizzes))
                        @foreach($quizzes as $quiz)
                            <input type="hidden" id="quiz_name_{{$quiz->point_id}}" name="quiz_name_{{$quiz->point_id}}" value="{{$quiz->name}}">
                            <input type="hidden" id="quiz_description_{{$quiz->point_id}}" name="quiz_description_{{$quiz->point_id}}" value="{{$quiz->description}}">
                            <input type="hidden" id="quiz_question_{{$quiz->point_id}}" name="quiz_question_{{$quiz->point_id}}" value="{{$quiz->question}}">

                            @foreach($quiz->answers as $key => $answer)
                                <input type="hidden" id="quiz_answer_{{$quiz->point_id}}_{{($key+1)}}" name="quiz_answer_{{$quiz->point_id}}_{{($key+1)}}" value="{{$answer->answer}}">
                                @if($answer->correct > 0)
                                    <input type="hidden" id="quiz_answer_right_{{$quiz->point_id}}_{{($key+1)}}" name="quiz_answer_right_{{$quiz->point_id}}_{{($key+1)}}" value="on">
                                @endif
                            @endforeach

                            @if ($quiz->hint)
                                <input type="hidden" id="quiz_hint_{{$quiz->point_id}}" name="quiz_hint_{{$quiz->point_id}}" value="{{$quiz->hint}}">
                            @endif

                            @if ($quiz->hint_point_lat && $quiz->hint_point_lng)
                                <input type="hidden" id="quiz_hint_point_lat_{{$quiz->point_id}}" name="quiz_hint_point_lat_{{$quiz->point_id}}" value="{{$quiz->hint_point_lat}}">
                                <input type="hidden" id="quiz_hint_point_lng_{{$quiz->point_id}}" name="quiz_hint_point_lng_{{$quiz->point_id}}" value="{{$quiz->hint_point_lng}}">
                            @endif
                        @endforeach
                    @endif
                </div>

                <hr />

                <button class="btn btn-default" id="cancel_btn" onclick="goBack(); return false;">Zrušit</button>
                <button type="submit" id="submit_btn" class="btn btn-primary btn-raised">Uložit trasu</button>
            </form>

            <!-- show_content_modal -->
            <div class="modal fade" id="show_content_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Spravovat obsah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <ul class="list-group pb-0" id="contents_list">
                                <li>
                                    <p>Zatím nebyl přidán žádný obsah.</p>
                                </li>
                            </ul>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                        </div>
                    </div>
                </div>
            </div>

            <!-- add_content_modal -->
            <div class="modal fade" id="add_content_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Přidat obsah</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form>
                            <div class="modal-body">
                                <p>Obsah se uživatelům procházejícím trasu zobrazí až při navštívení daného bodu.</p>

                                <div class="form-group">
                                    <label for="content_name" class="bmd-label-static">Název obsahu</label>
                                    <input type="text" class="form-control" id="content_name" name="content_name" required>
                                    <span class="bmd-help">Pojmenujte přidávaný obsah.</span>
                                </div>

                                <div class="form-group">
                                    <label for="content_type" class="bmd-label-static">Typ obsahu</label>
                                    <select class="form-control" id="content_type" name="content_type" required>
                                        <option value="1">Obrázek</option>
                                        <option value="2">Text</option>
                                        <option value="3">Zvuk</option>
                                    </select>
                                </div>

                                <div class="form-group" id="content_image_wrapper">
                                    <label for="" class="bmd-label-static">Nahrát obrázek</label>
                                    <input type="file" class="form-control-file" id="content_image" accept=".jpg,.png">
                                    <small class="text-muted">Vyberte obrázek ve formátu jpg či png, s velikostí do 2MB.</small>
                                </div>

                                <div class="form-group" id="content_text_wrapper">
                                    <label for="content_text" class="bmd-label-static">Zadejte text</label>
                                    <textarea class="form-control" id="content_text" rows="3" required></textarea>
                                </div>

                                <div class="form-group" id="content_sound_wrapper">
                                    <label for="" class="bmd-label-static">Nahrát zvuk</label>
                                    <input type="file" class="form-control-file" id="content_sound" accept=".mp3">
                                    <small class="text-muted">Vyberte zvuk ve formátu mp3, s velikostí do 2MB.</small>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                                <button type="button" class="btn btn-primary" id="point_content_save_btn" data-dismiss="modal">Uložit obsah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- add_quiz_modal -->
            <div class="modal fade" id="add_quiz_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Přidat/upravit kvíz</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Kvíz se uživatelům procházejícím trasu zobrazí až při navštívení daného bodu.</p>

                            <div class="form-group">
                                <label for="quiz_name" class="bmd-label-static">Název kvízu</label>
                                <input type="text" class="form-control" id="quiz_name" name="quiz_name" required>
                                <span class="bmd-help">Pojmenujte přidávaný kvíz.</span>
                            </div>

                            <div class="form-group">
                                <label for="quiz_description" class="bmd-label-static">Popis kvízu</label>
                                <textarea class="form-control" id="quiz_description" rows="3" required></textarea>
                            </div>

                            <div class="form-group">
                                <label for="quiz_question" class="bmd-label-static">Kvízová otázka</label>
                                <input type="text" class="form-control" id="quiz_question" name="quiz_question" required>
                                <span class="bmd-help">Zadejte kvízovou otázku.</span>
                            </div>

                            <p class="font-weight-bold pt-2">Odpovědi</p>
                            <p>Je možné zadat maximálně 4 možné odpovědi.</p>
                            <p>Jako správnou odpověď lze označit libovolný počet odpovědí.</p>

                            <button type="button" class="btn btn-primary" onclick="addNewAnswer();">Přidat odpověď</button>

                            <button type="button" class="btn btn-danger" onclick="removeAnswer();">Odebrat odpověď</button>

                            <div id="quiz_answers_wrapper"></div>

                            <p class="font-weight-bold pt-4">Akce při nesplnění kvízu:</p>

                            <div class="radio">
                                <label>
                                    <input type="radio" name="quiz_failed_actions" id="quiz_failed_action_1" value="1" checked>
                                    Kvíz je možné opakovat.
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="quiz_failed_actions" id="quiz_failed_action_2" value="2">
                                    Zobrazit nápovědu přímo na tomto bodě.
                                </label>
                            </div>
                            <div class="radio disabled">
                                <label>
                                    <input type="radio" name="quiz_failed_actions" id="quiz_failed_action_3" value="3">
                                    Zobrazit uživateli nápovědu a odkázat ho na bod, kde získá více informací.
                                </label>
                            </div>

                            <div class="form-group" id="quiz_hint_wrapper">
                                <label for="quiz_hint" class="bmd-label-static">Nápověda</label>
                                <textarea class="form-control" id="quiz_hint" rows="3" required></textarea>
                            </div>

                            <div class="form-group" id="point_with_hint_address_wrapper">
                                <input type="hidden" name="point_with_hint_lat" id="point_with_hint_lat" />
                                <input type="hidden" name="point_with_hint_lng" id="point_with_hint_lng" />
                                <label for="point_with_hint_address" class="bmd-label-static">Bod s nápovědou</label>
                                <input type="text" class="form-control" name="point_with_hint_address" id="point_with_hint_address" placeholder="" required>
                                <small class="text-muted">Pomocí nápovědy vyberte adresu bodu.</small>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                            <button type="button" class="btn btn-primary" id="point_quiz_save_btn" data-dismiss="modal">Uložit kvíz</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&libraries=places&callback=initAutocomplete">
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js" integrity="sha512-UdIMMlVx0HEynClOIFSyOrPggomfhBKJE28LKl8yR3ghkgugPnG6iLfRfHwushZl1MOPSY6TsuBDGPK2X4zYKg==" crossorigin="anonymous"></script>

    <script>
        function goBack() {
            window.location.href = "{{ route('my_routes')}}";
        }

        function initAutocomplete() {
            initMap();

            var input = document.getElementById('point_address');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.setFields(['geometry']);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (!place.geometry) {
                    window.alert("Žádné data o poloze '" + place.name + "' nebyla nalezena.");
                    return;
                }

                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();

                $("input[name=point_lat]").val(lat);
                $("input[name=point_lng]").val(lng);
            });
            autocomplete.setOptions({
                //types: ['address'],
                componentRestrictions: {country: ['CZ', 'SK']}
            });

            var input2 = document.getElementById('point_with_hint_address');
            var autocomplete2 = new google.maps.places.Autocomplete(input2);
            autocomplete2.setFields(['geometry']);
            autocomplete2.addListener('place_changed', function() {
                var place = autocomplete2.getPlace();

                if (!place.geometry) {
                    window.alert("Žádné data o poloze '" + place.name + "' nebyla nalezena.");
                    return;
                }

                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();

                $("input[name=point_with_hint_lat]").val(lat);
                $("input[name=point_with_hint_lng]").val(lng);
            });
            autocomplete2.setOptions({
                componentRestrictions: {country: ['CZ', 'SK']}
            });
        }

        function initMap() {
            const myLatlng = { lat: 49.876, lng: 15.493 };
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 6,
                center: myLatlng,
                disableDefaultUI: true,
                fullscreenControl: true,
                zoomControl: true
            });

            let infoWindow = new google.maps.InfoWindow({
                content: "Klikněte na mapě na místo, jehož souřadnice chcete získat.",
                position: myLatlng,
            });
            infoWindow.open(map);

            map.addListener("click", (mapsMouseEvent) => {

                infoWindow.close();

                infoWindow = new google.maps.InfoWindow({
                    position: mapsMouseEvent.latLng,
                });
                infoWindow.setContent(
                    JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
                );
                infoWindow.open(map);

                const lat = mapsMouseEvent.latLng.toJSON().lat;
                const lng = mapsMouseEvent.latLng.toJSON().lng;

                $("#point_lat").val(lat);
                $("#point_lng").val(lng);
            });
        }

        function refreshItemsList(items) {
            localStorage.setItem('route_items', JSON.stringify(items));

            $('#items_list').empty();

            items.forEach(async function(item) {
                var itemPublicText = 'doporučený';
                if (item.required === 1) {
                    itemPublicText = 'povinný';
                }

                $('<li class="d-flex"><a class="list-group-item">'+
                    '<p class="list-group-item-heading"><i class="material-icons">luggage</i><span>'+item.name+'</span></p><div class="bmd-list-group-col"><p class="list-group-item-text">'+itemPublicText+'</p></div>'+
                    '</a><div class="btn-group"><button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akce</button><div class="dropdown-menu"><a class="dropdown-item" href="#" onclick="editItem('+item.id+')">Upravit</a><a class="dropdown-item" href="#" onclick="removeItem('+item.id+')">Smazat</a></div></div></li>').appendTo("#items_list");
            })
        }

        function refreshPointsList(points) {
            localStorage.setItem('route_points', JSON.stringify(points));

            $('#points_list').empty();

            points.forEach(async function(point) {
                var pointLocation = point.lat + ', ' + point.lng;
                if (point.address !== '') {
                    pointLocation = point.address;
                }

                if (navigator.userAgent.match(/(iPod|iPhone|iPad|Mac)/)) {
                    $('<li id="point_'+point.id+'"><div class="d-flex"><a class="list-group-item">'+
                        '<div class="bmd-list-group-col">'+
                        '<p class="list-group-item-heading"><i class="material-icons">place</i><span>'+point.name+'</span></p><p class="list-group-item-text">'+pointLocation+'</p></div>'+
                        '</a><div class="btn-group"><button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akce</button>'+
                        '<div class="dropdown-menu"> <a class="dropdown-item" href="#" onclick="addContent('+point.id+')">Přidat obsah</a> <a class="dropdown-item" href="#" onclick="showContents('+point.id+')" data-toggle="modal" data-target="#show_content_modal">Spravovat obsah</a>'+
                        '<a class="dropdown-item" href="#" onclick="addOrEditQuiz('+point.id+')" data-toggle="modal" data-target="#add_quiz_modal">Přidat/upravit kvíz</a><a class="dropdown-item" href="#" onclick="removeQuiz('+point.id+', 1)">Odebrat kvíz</a>'+
                        '<a class="dropdown-item" href="#" onclick="editPoint('+point.id+')">Upravit bod</a><a class="dropdown-item" href="#" onclick="removePoint('+point.id+')">Smazat bod</a></div></div></div></li>').appendTo("#points_list");
                } else {
                    $('<li id="point_'+point.id+'"><div class="d-flex"><a class="list-group-item">'+
                        '<div class="bmd-list-group-col">'+
                        '<p class="list-group-item-heading"><i class="material-icons">place</i><span>'+point.name+'</span></p><p class="list-group-item-text">'+pointLocation+'</p></div>'+
                        '</a><div class="btn-group"><button class="btn dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Akce</button>'+
                        '<div class="dropdown-menu"> <a class="dropdown-item" href="#" onclick="addContent('+point.id+')" data-toggle="modal" data-target="#add_content_modal">Přidat obsah</a> <a class="dropdown-item" href="#" onclick="showContents('+point.id+')" data-toggle="modal" data-target="#show_content_modal">Spravovat obsah</a>'+
                        '<a class="dropdown-item" href="#" onclick="addOrEditQuiz('+point.id+')" data-toggle="modal" data-target="#add_quiz_modal">Přidat/upravit kvíz</a><a class="dropdown-item" href="#" onclick="removeQuiz('+point.id+', 1)">Odebrat kvíz</a>'+
                        '<a class="dropdown-item" href="#" onclick="editPoint('+point.id+')">Upravit bod</a><a class="dropdown-item" href="#" onclick="removePoint('+point.id+')">Smazat bod</a></div></div></div></li>').appendTo("#points_list");
                }
            })
        }

        function editItem(id) {
            var items = JSON.parse(localStorage.getItem('route_items'));

            for (var i=0; i < items.length; i++) {
                if (items[i].id === id) {
                    $("#item_name").val(items[i].name);
                    if (items[i].required === 1) {
                        $( "#item_required" ).prop( "checked", true );
                    } else {
                        $( "#item_required" ).prop( "checked", false );
                    }

                    var updated_items = items.filter(function(value, index, arr){
                        return value != items[i];
                    });

                    refreshItemsList(updated_items);
                }
            }
        }

        function removeItem(id) {
            var items = JSON.parse(localStorage.getItem('route_items'));

            for (var i=0; i < items.length; i++) {
                if (items[i].id === id) {
                    var updated_items = items.filter(function(value, index, arr){
                        return value != items[i];
                    });

                    refreshItemsList(updated_items);
                }
            }
        }

        function editPoint(id) {
            var points = JSON.parse(localStorage.getItem('route_points'));

            for (var i=0; i < points.length; i++) {
                if (points[i].id === id) {
                    $( "#add_point_btn" ).val(points[i].id);
                    $("#point_name").val(points[i].name);
                    $("#point_description").val(points[i].description);

                    if (points[i].address !== '') {
                        $( "#point_address_or_coordination" ).prop( "checked", false );
                        $("#point_address").val(points[i].address);
                    } else {
                        $( "#point_address_or_coordination" ).prop( "checked", true );
                    }

                    $("#point_lat").val(points[i].lat);
                    $("#point_lng").val(points[i].lng);

                    $("#point_entrance_fee").val(points[i].entrance_fee);
                }
            }
        }

        function removePoint(id) {
            var points = JSON.parse(localStorage.getItem('route_points'));

            for (var i=0; i < points.length; i++) {
                if (points[i].id === id) {
                    var updated_points = points.filter(function(value, index, arr){
                        return value != points[i];
                    });

                    refreshPointsList(updated_points);
                }
            }
        }

        var itemsInit = [];
        @if(isset($route->items))
            localStorage.setItem('route_items', @json($items));
            refreshItemsList(JSON.parse(localStorage.getItem('route_items')));
        @else
            localStorage.setItem('route_items', JSON.stringify(itemsInit));
        @endif

        $( "#add_item_btn" ).click(function() {
            if( $( "#item_name" ).val() ) {
                var itemsListLength = $( "#items_list li" ).length;

                if (itemsListLength < 10) {
                    var itemName = $('#item_name').val().replace(/(<([^>]+)>)/gi, "");
                    var itemRequiredInput = $('#item_required');
                    var itemRequired = 0;

                    if (itemRequiredInput.is(':checked')) {
                        itemRequired = 1;
                    }

                    var items = JSON.parse(localStorage.getItem('route_items'));

                    if (items.length > 0) {
                        const lastItemId = items[items.length - 1].id;
                        var item = {name: itemName, required: itemRequired, id: (lastItemId+1)};
                    } else {
                        var item = {name: itemName, required: itemRequired, id: items.length};
                    }

                    items.push(item);

                    $( "#item_name" ).val('');
                    $( "#item_required" ).prop( "checked", false );

                    refreshItemsList(items);

                    Snackbar.show({
                        text: 'Předmět byl úspěšně uložen.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                } else {
                    Snackbar.show({
                        text: 'Lze přidat maximálně 10 předmětů.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                }
            } else {
                Snackbar.show({
                    text: 'Zadejte prosím název předmětu.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        });

        jQuery.extend(jQuery.validator.messages, {
            required: "Toto pole je povinné."
        });

        $("#route_form").validate({
            submitHandler: function(form) {
                var items = localStorage.getItem('route_items');
                var points = localStorage.getItem('route_points');
                var pointsArray = JSON.parse(points);

                if (pointsArray.length >= 2) {
                    $('#submit_btn').prop('disabled', true);

                    $('<input>').attr({
                        type: 'hidden',
                        id: 'items',
                        name: 'items',
                        value: items
                    }).appendTo('#route_form');

                    $('<input>').attr({
                        type: 'hidden',
                        id: 'points',
                        name: 'points',
                        value: points
                    }).appendTo('#route_form');

                    $("#route_form")[0].submit();
                } else {
                    Snackbar.show({
                        text: 'Musíte zadat alespoň dva body, aby bylo možné trasu uložit.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                }
            }
        });

        var pointsInit = [];
        @if(isset($route->points))
            localStorage.setItem('route_points', @json($points));
            refreshPointsList(JSON.parse(localStorage.getItem('route_points')));
        @else
            localStorage.setItem('route_points', JSON.stringify(pointsInit));
        @endif

        $( "#add_point_btn" ).click(function() {
            if( $( "#point_name" ).val() && $( "#point_description" ).val() && $( "#point_lat" ).val() && $( "#point_lng" ).val()) {
                if ($( "#point_entrance_fee" ).val() !== '' && ($( "#point_entrance_fee" ).val() < 0 || $( "#point_entrance_fee" ).val() > 10000)) {
                    Snackbar.show({
                        text: 'Vstupné musí být zadáno v číselném rozsahu 0 až 10000.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                } else {
                    var pointsListLength = $( "#points_list li" ).length;

                    if (pointsListLength < 10) {
                        const pointId = $( "#add_point_btn" ).val();
                        var points = JSON.parse(localStorage.getItem('route_points'));
                        var pointName = $('#point_name').val().replace(/(<([^>]+)>)/gi, "");
                        var pointDescription = $('#point_description').val().replace(/(<([^>]+)>)/gi, "");
                        var pointAddress = $('#point_address').val().replace(/(<([^>]+)>)/gi, "");
                        var pointLat = $('#point_lat').val();
                        var pointLng = $('#point_lng').val();
                        var pointEntranceFee = $('#point_entrance_fee').val();

                        if (pointId !== '') {
                            for (var i in points) {
                                if (points[i].id == pointId) {
                                    points[i].name = pointName;
                                    points[i].description = pointDescription;
                                    points[i].address = pointAddress;
                                    points[i].lat = pointLat;
                                    points[i].lng = pointLng;
                                    points[i].entrance_fee = pointEntranceFee;
                                    break;
                                }
                            }
                        } else {
                            if (points.length > 0) {
                                const lastPointId = points[points.length - 1].id;
                                var point = {id: (lastPointId+1), name: pointName, description: pointDescription, address: pointAddress, lat: pointLat, lng: pointLng, entrance_fee: pointEntranceFee};
                            } else {
                                var point = {id: points.length, name: pointName, description: pointDescription, address: pointAddress, lat: pointLat, lng: pointLng, entrance_fee: pointEntranceFee};
                            }

                            points.push(point);
                        }

                        $( "#point_name" ).val('');
                        $( "#point_description" ).val('');
                        $( "#point_address" ).val('');
                        $( "#point_lat" ).val('');
                        $( "#point_lng" ).val('');
                        $( "#point_entrance_fee" ).val('');
                        $( "#point_address_or_coordination" ).prop( "checked", false );

                        refreshPointsList(points);

                        Snackbar.show({
                            text: 'Bod byl úspěšně uložen.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    } else {
                        Snackbar.show({
                            text: 'Lze přidat maximálně 10 bodů.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    }
                }
            } else {
                Snackbar.show({
                    text: 'Pokud chcete přidat bod trasy, vyplňte jeho název, popis a pomocí nápovědy vyberte příslušnou adresu (či alespoň zadejte souřadnice).',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        });

        function addContent(id) {
            if (navigator.userAgent.match(/(iPod|iPhone|iPad|Mac)/)) {
                Snackbar.show({
                    text: 'Obsah lze přidávat pouze při tvorbě trasy za využití zařízení s operačním systémem Android či Windows.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            } else {
                $("#point_content_save_btn").prop('value', id);
            }

        }

        function showContents(id) {
            $('#contents_list').empty();

            var contentImage = $('#content_image_'+id);
            var contentImageName = $('#content_image_name_'+id);
            var existingContentImageId = $('#existing_content_image_id_'+id);
            var contentText = $('#content_text_'+id);
            var contentTextName = $('#content_text_name_'+id);
            var existingContentTextId = $('#existing_content_text_id_'+id);
            var contentSound = $('#content_sound_'+id);
            var contentSoundName = $('#content_sound_name_'+id);
            var existingContentSoundId = $('#existing_content_sound_id_'+id);

            if( (existingContentImageId.length || contentImage.length) && contentImageName.length ) {
                $('<li class="d-flex"><a class="list-group-item"><div class="row">'+
                   '<div class="col-50"><div class="row"><div class="col-30"> <i class="material-icons">image</i> </div> <div class="col-70"> <div class="bmd-list-group-col"> <p class="list-group-item-heading">'+contentImageName.val()+'</p><p class="list-group-item-text">Obrázek</p> </div> </div> </div> </div>'+
                    '<div class="col-50"><button type="button" class="btn btn-danger btn-raised ml-3" onclick="removeContentImage('+id+');">Odebrat</button> </div>'+
                    '</div> </a> </li>').appendTo("#contents_list");
            }

            if( (existingContentTextId.length || contentText.length) && contentTextName.length ) {
                $('<li class="d-flex"><a class="list-group-item"><div class="row">'+
                    '<div class="col-50"><div class="row"><div class="col-30"> <i class="material-icons">article</i> </div> <div class="col-70"> <div class="bmd-list-group-col"> <p class="list-group-item-heading">'+contentTextName.val()+'</p><p class="list-group-item-text">Text</p> </div> </div> </div> </div>'+
                    '<div class="col-50"><button type="button" class="btn btn-danger btn-raised ml-3" onclick="removeContentText('+id+');">Odebrat</button> </div>'+
                    '</div> </a> </li>').appendTo("#contents_list");
            }

            if( (existingContentSoundId.length || contentSound.length) && contentSoundName.length ) {
                $('<li class="d-flex"><a class="list-group-item"><div class="row">'+
                    '<div class="col-50"><div class="row"><div class="col-30"> <i class="material-icons">music_note</i> </div> <div class="col-70"> <div class="bmd-list-group-col"> <p class="list-group-item-heading">'+contentSoundName.val()+'</p><p class="list-group-item-text">Zvuk</p> </div> </div> </div> </div>'+
                    '<div class="col-50"><button type="button" class="btn btn-danger btn-raised ml-3" onclick="removeContentSound('+id+');">Odebrat</button> </div>'+
                    '</div> </a> </li>').appendTo("#contents_list");
            }

            if( (contentImage.length === 0 && contentImageName.length === 0) && (contentText.length === 0 && contentTextName.length === 0) && (contentSound.length === 0 && contentSoundName.length === 0) ) {
                $('<li> <p>Zatím nebyl přidán žádný obsah.</p> </li>').appendTo("#contents_list");
            }
        }

        function removeContentImage(id) {
            var contentImage = $('#content_image_'+id);
            var contentImageName = $('#content_image_name_'+id);
            var existingContentImageId = $('#existing_content_image_id_'+id);

            if( contentImage.length && contentImageName.length ) {
                contentImage.remove();
                contentImageName.remove();

                showContents(id);
            }

            if (existingContentImageId.length && contentImageName.length) {
                contentImageName.remove();

                showContents(id);
            }
        }

        function removeContentText(id) {
            var contentText = $('#content_text_'+id);
            var contentTextName = $('#content_text_name_'+id);
            var existingContentTextId = $('#existing_content_text_id_'+id);

            if( contentText.length && contentTextName.length ) {
                contentText.remove();
                contentTextName.remove();

                showContents(id);
            }

            if (existingContentTextId.length && contentTextName.length) {
                existingContentTextId.remove();
                contentTextName.remove();

                showContents(id);
            }
        }

        function removeContentSound(id) {
            var contentSound = $('#content_sound_'+id);
            var contentSoundName = $('#content_sound_name_'+id);
            var existingContentSoundId = $('#existing_content_sound_id_'+id);

            if( contentSound.length && contentSoundName.length ) {
                contentSound.remove();
                contentSoundName.remove();

                showContents(id);
            }

            if (existingContentSoundId.length && contentSoundName.length) {
                contentSoundName.remove();

                showContents(id);
            }
        }

        function removeContents(id) {
            if($('#content_image_'+id).length) {
                $('#content_image_'+id).remove();
            }

            if ($('#content_text_'+id).length) {
                $('#content_text_'+id).remove();
            }

            if ($('#content_sound_'+id).length) {
                $('#content_sound_'+id).remove();
            }
        }

        function addOrEditQuiz(id) {
            $("#quiz_name").val('');
            $("#quiz_description").val('');
            $("#quiz_question").val('');
            $("#quiz_answers_wrapper").html('');
            $("#quiz_failed_action_1").prop('checked', true);
            $("#quiz_failed_action_2").prop('checked', false);
            $("#quiz_failed_action_3").prop('checked', false);
            $("#quiz_hint").val('');
            $("#point_with_hint_address").val('');
            $("#quiz_hint_wrapper").hide();
            $("#point_with_hint_address_wrapper").hide();

            $("#point_quiz_save_btn").prop('value', id);
            var existingQuizNameElement = $("#quiz_name_"+id);
            var existingQuizDescriptionElement = $("#quiz_description_"+id);
            var existingQuizQuestionElement = $("#quiz_question_"+id);

            if (existingQuizNameElement.length && existingQuizDescriptionElement.length && existingQuizQuestionElement.length) {
                $("#quiz_answers_wrapper").html('');

                $("#quiz_name").val(existingQuizNameElement.val());
                $("#quiz_description").val(existingQuizDescriptionElement.val());
                $("#quiz_question").val(existingQuizQuestionElement.val());

                var existingQuizHintElement = $("#quiz_hint_"+id);
                var existingQuizLatElement = $("#quiz_hint_point_lat_"+id);
                var existingQuizLngElement = $("#quiz_hint_point_lng_"+id);

                if (existingQuizHintElement.length && existingQuizLatElement.length && existingQuizLngElement.length) {
                    $("#quiz_hint").val(existingQuizHintElement.val());
                    $("#point_with_hint_lat").val(existingQuizLatElement.val());
                    $("#point_with_hint_lng").val(existingQuizLngElement.val());

                    $("#quiz_failed_action_3").prop("checked", true);
                    $("#point_with_hint_address").val(existingQuizLatElement.val()+', '+existingQuizLngElement.val());
                    $("#point_with_hint_address_wrapper").show();
                    $("#quiz_hint_wrapper").show();
                } else if (existingQuizHintElement.length) {
                    $("#quiz_hint").val(existingQuizHintElement.val());

                    $("#quiz_failed_action_2").prop("checked", true);
                    $("#quiz_hint_wrapper").show();
                }

                var i;
                for (i = 1; i < 5; i++) {
                    var existingAnswerElement = $("#quiz_answer_"+id+"_"+i);
                    var existingRightAnswerElement = $("#quiz_answer_right_"+id+"_"+i);
                    if (existingAnswerElement.length && existingRightAnswerElement.length) {
                        $( '<div class="quiz_answer_wrapper"><div class="col-7 pt-4 pb-4"><div class="form-group bmd-form-group"> <label for="quiz_answer_'+i+'" class="bmd-label-static">Kvízová odpověď</label> <input type="text" class="form-control" id="quiz_answer_'+i+'" name="quiz_answer_'+i+'" value="'+existingAnswerElement.val()+'" required> <span class="bmd-help">Zadejte možnou odpověď na kvízovou otázku.</span> </div> </div>'+
                            '<div class="col pt-5"> <span class="bmd-form-group is-filled"><div class="checkbox"> <label> <input type="checkbox" name="quiz_answer_right_'+i+'" id="quiz_answer_right_'+i+'" checked><span class="checkbox-decorator"><span class="check"></span><div class="ripple-container"></div></span> Správná odpověď </label> </div></span> </div> </div>' ).appendTo( "#quiz_answers_wrapper" );
                    } else if (existingAnswerElement.length) {
                        $( '<div class="quiz_answer_wrapper"><div class="col-7 pt-4 pb-4"><div class="form-group bmd-form-group"> <label for="quiz_answer_'+i+'" class="bmd-label-static">Kvízová odpověď</label> <input type="text" class="form-control" id="quiz_answer_'+i+'" name="quiz_answer_'+i+'" value="'+existingAnswerElement.val()+'" required> <span class="bmd-help">Zadejte možnou odpověď na kvízovou otázku.</span> </div> </div>'+
                            '<div class="col pt-5"> <span class="bmd-form-group is-filled"><div class="checkbox"> <label> <input type="checkbox" name="quiz_answer_right_'+i+'" id="quiz_answer_right_'+i+'"><span class="checkbox-decorator"><span class="check"></span><div class="ripple-container"></div></span> Správná odpověď </label> </div></span> </div> </div>' ).appendTo( "#quiz_answers_wrapper" );
                    }
                }
            }
        }

        function removeElement(element) {
            if (element.length) {
                element.remove();
            }
        }

        function removeQuiz(id, message) {
            const quizName = $('#quiz_name_'+id);
            const quizDescription = $('#quiz_description_'+id);
            const quizQuestion = $('#quiz_question_'+id);
            const quizHint = $('#quiz_hint_'+id);
            const quizHintPointLat = $('#quiz_hint_point_lat_'+id);
            const quizHintPointLng = $('#quiz_hint_point_lng_'+id);

            if (message === 1) {
                if (quizName.length === 0) {
                    Snackbar.show({
                        text: 'Tento bod nemá přiřazený žádný kvíz.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                } else {
                    Snackbar.show({
                        text: 'Kvíz úspěšně odebrán.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                }
            }

            removeElement(quizName);
            removeElement(quizDescription);
            removeElement(quizQuestion);
            removeElement(quizHint);
            removeElement(quizHintPointLat);
            removeElement(quizHintPointLng);

            var i;
            for (i = 0; i < 5; i++) {
                const quizAnswer = $('#quiz_answer_'+id+'_'+i);
                const quizAnswerRight = $('#quiz_answer_right_'+id+'_'+i);
                removeElement(quizAnswer);
                removeElement(quizAnswerRight);
            }
        }

        function addNewAnswer() {
            var numberOfExistingAnswers = $(".quiz_answer_wrapper").length;
            var idOfNewAnswer = numberOfExistingAnswers + 1;

            if (numberOfExistingAnswers < 4) {
                $( '<div class="quiz_answer_wrapper"><div class="col-7 pt-4 pb-4"><div class="form-group bmd-form-group"> <label for="quiz_answer_'+idOfNewAnswer+'" class="bmd-label-static">Kvízová odpověď</label> <input type="text" class="form-control" id="quiz_answer_'+idOfNewAnswer+'" name="quiz_answer_'+idOfNewAnswer+'" required> <span class="bmd-help">Zadejte možnou odpověď na kvízovou otázku.</span> </div> </div>'+
                    '<div class="col pt-5"> <span class="bmd-form-group is-filled"><div class="checkbox"> <label> <input type="checkbox" name="quiz_answer_right_'+idOfNewAnswer+'" id="quiz_answer_right_'+idOfNewAnswer+'"><span class="checkbox-decorator"><span class="check"></span><div class="ripple-container"></div></span> Správná odpověď </label> </div></span> </div> </div>' ).appendTo( "#quiz_answers_wrapper" );
            } else {
                Snackbar.show({
                    text: 'Již jste přidal maximální počet odpovědí.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        }

        function removeAnswer() {
            var numberOfExistingAnswers = $(".quiz_answer_wrapper").length;

            if (numberOfExistingAnswers > 0) {
                $("#quiz_answers_wrapper .quiz_answer_wrapper").last().remove();
            }
        }

        $( document ).ready(function() {
            $(window).keydown(function(event){
                if(event.keyCode === 13) {
                    event.preventDefault();
                    return false;
                }
            });

            $( "#point_address_or_coordination" ).change(function() {
                if ($( "#point_address_or_coordination" ).prop('checked')) {
                    $("#point_address_wrapper").hide();
                    $("#point_lat_lng_wrapper").show();
                } else {
                    $("#point_address_wrapper").show();
                    $("#point_lat_lng_wrapper").hide();
                }
            });

            $( "#point_address_or_coordination" ).click(function() {
                $("#point_address").val('');
                $("#point_lat_lng_wrapper input").val('');
            });

            $( "#content_type" ).change(function() {
                let contentType = $( "#content_type" ).val();

                if (contentType === '1') {
                    $("#content_image_wrapper").show();
                    $("#content_text_wrapper").hide();
                    $("#content_sound_wrapper").hide();
                } else if (contentType === '2') {
                    $("#content_image_wrapper").hide();
                    $("#content_text_wrapper").show();
                    $("#content_sound_wrapper").hide();
                } else if (contentType === '3') {
                    $("#content_image_wrapper").hide();
                    $("#content_text_wrapper").hide();
                    $("#content_sound_wrapper").show();
                }
            });

            if($('#public_0').is(':checked')) {
                $("#enable_links_wrapper").show();
            }

            $('#public_0').click(function() {
                $("#enable_links_wrapper").show();
            });

            $('#public_1').click(function() {
                $("#enable_links_wrapper").hide();
            });

            $("#point_content_save_btn").click(function() {
                var contentImageVal = $('#content_image').val();
                var contentSoundVal = $('#content_sound').val();
                var contentName = $('#content_name').val().replace(/(<([^>]+)>)/gi, "");
                var contentType = $( "#content_type" ).val();
                if (((contentImageVal === '' || contentName === '') && contentType === '1') || ((contentSoundVal  === ''  || contentName === '') && contentType === '3')) {
                    Snackbar.show({
                        text: 'Před uložením musíte vybrat soubor, který chcete přidat.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else if ((contentName === '' || $("#content_text").val() === '') && contentType === '2') {
                    Snackbar.show({
                        text: 'Před uložením musíte zadat název obsahu a samotný text.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else {
                    var pointId = $("#point_content_save_btn").val();
                    if (contentType === '1' && !$('#contents_wrapper #content_image_'+pointId).length) {
                        $("#content_image_wrapper").show();
                        $("#content_text_wrapper").hide();
                        $("#content_sound_wrapper").hide();

                        var original = $("#content_image"),
                            duplicate = original.clone();
                        duplicate.appendTo("#contents_wrapper");
                        duplicate.attr("id", "content_image_"+pointId);
                        duplicate.attr("name", "content_image_"+pointId);

                        original.val('');

                        $('<input type="text" id="content_image_name_'+pointId+'" name="content_image_name_'+pointId+'" value="'+contentName+'">').appendTo( "#contents_wrapper" );

                        Snackbar.show({
                            text: 'Obsah byl úspěšně přidán.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    } else if (contentType === '2' && !$('#contents_wrapper #content_text_'+pointId).length) {
                        $("#content_image_wrapper").hide();
                        $("#content_text_wrapper").show();
                        $("#content_sound_wrapper").hide();

                        var original = $("#content_text"),
                            duplicate = original.clone();
                        duplicate.appendTo("#contents_wrapper");
                        duplicate.attr("id", "content_text_"+pointId);
                        duplicate.attr("name", "content_text_"+pointId);

                        original.val('');

                        $('<input type="text" id="content_text_name_'+pointId+'" name="content_text_name_'+pointId+'" value="'+contentName+'">').appendTo( "#contents_wrapper" );

                        Snackbar.show({
                            text: 'Obsah byl úspěšně přidán.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    } else if (contentType === '3' && !$('#contents_wrapper #content_sound_'+pointId).length) {
                        $("#content_image_wrapper").hide();
                        $("#content_text_wrapper").hide();
                        $("#content_sound_wrapper").show();

                        var original = $("#content_sound"),
                            duplicate = original.clone();
                        duplicate.appendTo("#contents_wrapper");
                        duplicate.attr("id", "content_sound_"+pointId);
                        duplicate.attr("name", "content_sound_"+pointId);

                        original.val('');

                        $('<input type="text" id="content_sound_name_'+pointId+'" name="content_sound_name_'+pointId+'" value="'+contentName+'">').appendTo( "#contents_wrapper" );

                        Snackbar.show({
                            text: 'Obsah byl úspěšně přidán.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    } else {
                        Snackbar.show({
                            text: 'Tento typ obsahu je již u daného bodu uložen.',
                            actionText: 'OK',
                            actionTextColor: '#4CAF50',
                            pos: 'bottom-center'
                        });
                    }

                    $('#content_image').val('');
                    $("#content_name").val('');
                    $('#content_sound').val('');
                    $("#content_type option:selected").prop("selected", false)
                    $("#content_image_wrapper").show();
                    $("#content_text_wrapper").hide();
                    $("#content_sound_wrapper").hide();
                }
            });

            $('input[type=radio][id=quiz_failed_action_1]').click(function() {
                $("#point_with_hint_address_wrapper").hide();
                $("#quiz_hint_wrapper").hide();
            });

            $('input[type=radio][id=quiz_failed_action_2]').click(function() {
                $("#point_with_hint_address_wrapper").hide();
                $("#quiz_hint_wrapper").show();
            });

            $('input[type=radio][id=quiz_failed_action_3]').click(function() {
                $("#point_with_hint_address_wrapper").show();
                $("#quiz_hint_wrapper").show();
            });

            $("#point_quiz_save_btn").click(function() {
                var quizId = $("#point_quiz_save_btn").val(); //could also be named as pointId
                var quizName = $( "#quiz_name" ).val().replace(/(<([^>]+)>)/gi, "");
                var quizDescription = $( "#quiz_description" ).val().replace(/(<([^>]+)>)/gi, "");
                var quizQuestion = $( "#quiz_question" ).val().replace(/(<([^>]+)>)/gi, "");

                if (quizName === '' || quizDescription === '' || quizQuestion === '' || quizName === '') {
                    Snackbar.show({
                        text: 'Vyplňte název a popis kvízu, stejně tak jako kvízovou otázku.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else if ($('#quiz_failed_action_2').is(':checked') && $('#quiz_hint').val() === '') {
                    Snackbar.show({
                        text: 'Vyplňte pole nápovědy.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else if ($('#quiz_failed_action_3').is(':checked') && ($('#quiz_hint').val() === '' || ($('#point_with_hint_lat').val() === '' && $('#point_with_hint_lng').val() === ''))) {
                    Snackbar.show({
                        text: 'Vyplňte pole nápovědy a vyberte bod s nápovědou.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else if (($('#quiz_answer_1').length === 0 || $('#quiz_answer_2').length === 0) || ($('#quiz_answer_1').val() === '' || $('#quiz_answer_2').val() === '')) {
                    Snackbar.show({
                        text: 'Zadejte prosím alespoň dvě možné odpovědi.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else if ($('#quiz_answer_right_1').is(":checked") === false && $('#quiz_answer_right_2').is(":checked") === false && $('#quiz_answer_right_3').is(":checked") === false && $('#quiz_answer_right_4').is(":checked") === false) {
                    Snackbar.show({
                        text: 'Vyberte prosím alespoň jednu správnou odpověď.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });

                    return false;
                } else {
                    removeQuiz(quizId, 0);

                    $('<input type="text" id="quiz_name_'+quizId+'" name="quiz_name_'+quizId+'" value="'+quizName+'">').appendTo( "#quizzes_wrapper" );
                    $('<input type="text" id="quiz_description_'+quizId+'" name="quiz_description_'+quizId+'" value="'+quizDescription+'">').appendTo( "#quizzes_wrapper" );
                    $('<input type="text" id="quiz_question_'+quizId+'" name="quiz_question_'+quizId+'" value="'+quizQuestion+'">').appendTo( "#quizzes_wrapper" );

                    var quizHint = '';
                    if ($('#quiz_failed_action_2').is(':checked')) {
                        quizHint = $('#quiz_hint').val().replace(/(<([^>]+)>)/gi, "");
                        $('<input type="text" id="quiz_hint_'+quizId+'" name="quiz_hint_'+quizId+'" value="'+quizHint+'">').appendTo( "#quizzes_wrapper" );
                    } else if ($('#quiz_failed_action_3').is(':checked')) {
                        quizHint = $('#quiz_hint').val().replace(/(<([^>]+)>)/gi, "");
                        var quizHintPointLat = $('#point_with_hint_lat').val();
                        var quizHintPointLng = $('#point_with_hint_lng').val();

                        $('<input type="text" id="quiz_hint_'+quizId+'" name="quiz_hint_'+quizId+'" value="'+quizHint+'">').appendTo( "#quizzes_wrapper" );
                        $('<input type="text" id="quiz_hint_point_lat_'+quizId+'" name="quiz_hint_point_lat_'+quizId+'" value="'+quizHintPointLat+'">').appendTo( "#quizzes_wrapper" );
                        $('<input type="text" id="quiz_hint_point_lng_'+quizId+'" name="quiz_hint_point_lng_'+quizId+'" value="'+quizHintPointLng+'">').appendTo( "#quizzes_wrapper" );
                    }

                    if ($('#quiz_answer_1').length && $('#quiz_answer_1').val() !== '') {
                        $('<input type="text" id="quiz_answer_'+quizId+'_1" name="quiz_answer_'+quizId+'_1" value="'+$('#quiz_answer_1').val().replace(/(<([^>]+)>)/gi, "")+'">').appendTo( "#quizzes_wrapper" );
                        if ($('#quiz_answer_right_1').length && $('#quiz_answer_right_1').is(":checked")) {
                            $('<input type="text" id="quiz_answer_right_'+quizId+'_1" name="quiz_answer_right_'+quizId+'_1" value="'+$('#quiz_answer_right_1').val()+'">').appendTo( "#quizzes_wrapper" );
                        }
                    }

                    if ($('#quiz_answer_2').length && $('#quiz_answer_2').val() !== '') {
                        $('<input type="text" id="quiz_answer_'+quizId+'_2" name="quiz_answer_'+quizId+'_2" value="'+$('#quiz_answer_2').val().replace(/(<([^>]+)>)/gi, "")+'">').appendTo( "#quizzes_wrapper" );
                        if ($('#quiz_answer_right_2').length && $('#quiz_answer_right_2').is(":checked")) {
                            $('<input type="text" id="quiz_answer_right_'+quizId+'_2" name="quiz_answer_right_'+quizId+'_2" value="'+$('#quiz_answer_right_2').val()+'">').appendTo( "#quizzes_wrapper" );
                        }
                    }

                    if ($('#quiz_answer_3').length && $('#quiz_answer_3').val() !== '') {
                        $('<input type="text" id="quiz_answer_'+quizId+'_3" name="quiz_answer_'+quizId+'_3" value="'+$('#quiz_answer_3').val().replace(/(<([^>]+)>)/gi, "")+'">').appendTo( "#quizzes_wrapper" );
                        if ($('#quiz_answer_right_3').length && $('#quiz_answer_right_3').is(":checked")) {
                            $('<input type="text" id="quiz_answer_right_'+quizId+'_3" name="quiz_answer_right_'+quizId+'_3" value="'+$('#quiz_answer_right_3').val()+'">').appendTo( "#quizzes_wrapper" );
                        }
                    }

                    if ($('#quiz_answer_4').length && $('#quiz_answer_4').val() !== '') {
                        $('<input type="text" id="quiz_answer_'+quizId+'_4" name="quiz_answer_'+quizId+'_4" value="'+$('#quiz_answer_4').val().replace(/(<([^>]+)>)/gi, "")+'">').appendTo( "#quizzes_wrapper" );
                        if ($('#quiz_answer_right_4').length && $('#quiz_answer_right_4').is(":checked")) {
                            $('<input type="text" id="quiz_answer_right_'+quizId+'_4" name="quiz_answer_right_'+quizId+'_4" value="'+$('#quiz_answer_right_4').val()+'">').appendTo( "#quizzes_wrapper" );
                        }
                    }

                    $("#point_quiz_save_btn").val('');
                    $( "#quiz_name" ).val('');
                    $( "#quiz_description" ).val('');
                    $( "#quiz_question" ).val('');
                    $( "#quiz_hint" ).val('');
                    $( "#point_with_hint_address" ).val('');
                    $( "#point_with_hint_lat" ).val('');
                    $( "#point_with_hint_lng" ).val('');
                    $( "#quiz_answers_wrapper" ).html('');

                    $( "#quiz_failed_action_1" ).prop( "checked", true );
                    $( "#quiz_failed_action_2" ).prop( "checked", false );
                    $( "#quiz_failed_action_3" ).prop( "checked", false );
                    $("#point_with_hint_address_wrapper").hide();
                    $("#quiz_hint_wrapper").hide();

                    Snackbar.show({
                        text: 'Kvíz byl úspěšně přidán.',
                        actionText: 'OK',
                        actionTextColor: '#4CAF50',
                        pos: 'bottom-center'
                    });
                }
            });
        });

        $(document).on('focusin', '#quiz_answers_wrapper input', function(){
            if (!$(this).parent().hasClass("is-focused")) {
                $( this ).parent().addClass( "is-focused" );
            }
        });

        $('#content_image').bind('change', function() {
            var ext = $('#content_image').val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['gif','png','jpg','jpeg']) === -1) {
                $('#content_image').val('');

                Snackbar.show({
                    text: 'Vybírejte pouze soubory povoleného typu.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }

            if ((this.files[0].size/1000000) > 2) {
                $('#content_image').val('');

                Snackbar.show({
                    text: 'Soubor větší než 2 MB nelze nahrát.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        })

        $('#content_sound').bind('change', function() {
            var ext = $('#content_sound').val().split('.').pop().toLowerCase();

            if ($.inArray(ext, ['mp3']) === -1) {
                $('#content_sound').val('');

                Snackbar.show({
                    text: 'Vybírejte pouze soubory povoleného typu.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }

            if ((this.files[0].size/1000000) > 2) {
                $('#content_sound').val('');

                Snackbar.show({
                    text: 'Soubor větší než 2 MB nelze nahrát.',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center'
                });
            }
        })
    </script>
@endsection
