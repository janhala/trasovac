@extends('layouts.app')

@section('title', 'Import tras')

@section('head')
    <style>
        #teamwork {
            display: none;
        }

        .add-teamwork-btn {
            display: none;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <ul class="nav nav-tabs bg-primary w-100">
            <li class="nav-item mx-auto">
                <a class="nav-link active" href="#imported" id="imported_btn">IMPORTOVANÉ</a>
            </li>
            <li class="nav-item mx-auto">
                <a class="nav-link" href="#teamwork" id="teamwork_btn">SPOLUPRACUJI</a>
            </li>
        </ul>
    </div>

    <div class="row" id="imported">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mt-3" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div class="col-auto mt-3">
                <span>Importováno tras: {{$imported_routes_count}}</span>
            </div>

            <div class="list-group">
                @foreach($imported_routes as $route)
                    <a href="{{route('view_route', $route->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($route->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">importováno {{date('d.m.Y', strtotime($route->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($route->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-chart-line"></i>
                                    {{$route->difficulty}}
                                </span>
                            </div>

                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-route"></i>
                                    cca {{$route->distance}} km
                                </span>
                            </div>

                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-stopwatch"></i>
                                    @if((floor($route->estimated_time/60)))
                                        @if ((floor($route->estimated_time/60)) > 0)
                                            {{(floor($route->estimated_time/60))}} hod.
                                        @endif
                                        @if (($route->estimated_time%60) > 0)
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    @else
                                        {{($route->estimated_time%60)}} min.
                                    @endif
                                </span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row" id="teamwork">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mt-3" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div class="col-auto mt-3">
                <span>Spolupracuji na trasách: {{$teamwork_routes_count}}</span>
            </div>

            <div class="list-group">
                @foreach($teamwork_routes as $route)
                    <a href="{{route('view_route', $route->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{ \Illuminate\Support\Str::limit($route->name, 40, $end='...') }}
                            </h5>
                            <small class="pt-1 text-muted">importováno {{date('d.m.Y', strtotime($route->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($route->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-chart-line"></i>
                                    {{$route->difficulty}}
                                </span>
                            </div>

                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-route"></i>
                                    cca {{$route->distance}} km
                                </span>
                            </div>

                            <div class="p-2 mx-auto">
                                <span>
                                    <i class="fas fa-stopwatch"></i>
                                    @if((floor($route->estimated_time/60)))
                                        @if ((floor($route->estimated_time/60)) > 0)
                                            {{(floor($route->estimated_time/60))}} hod.
                                        @endif
                                        @if (($route->estimated_time%60) > 0)
                                            {{($route->estimated_time%60)}} min.
                                        @endif
                                    @else
                                        {{($route->estimated_time%60)}} min.
                                    @endif
                                </span>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <button type="button" class="btn btn-success bmd-btn-fab add-route-btn" id="import_route_btn" data-toggle="modal" data-target="#import_route_modal">
                <i class="material-icons">add</i>
            </button>

            <button type="button" class="btn btn-success bmd-btn-fab add-route-btn" id="add_teamwork_btn" data-toggle="modal" data-target="#add_teamwork_modal">
                <i class="material-icons">add</i>
            </button>
        </div>
    </div>

    <!-- import_route_modal -->
    <div class="modal fade" id="import_route_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <form method="POST" action="{{ route('import_route_num_code') }}">
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Importovat trasu pomocí číselného kódu</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="numeric_code" class="bmd-label-static">Číselný kód</label>
                            <input type="number" class="form-control" id="numeric_code" name="numeric_code" required>
                            <span class="bmd-help">Zadejte prosím získaný číselný kód.</span>
                        </div>
                        <p class="font-italic mt-4 pb-0 mb-0">Trasu také můžete importovat pomocí odkazu - daný odkaz stačí pouze načíst ve Vašem prohlížeči.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                        <button type="submit" class="btn btn-primary">Uložit změny</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- add_teamwork_modal -->
    <div class="modal fade" id="add_teamwork_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Přidání spolupráce</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="font-italic">Pokud Vás někdo pozval ke spolupráci na trase pomocí odkazu, stačí odkaz načíst ve webovém prohlížeči.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(window).on('hashchange', function(){
                var hash = location.hash;

                if (hash === '#teamwork') {
                    $("#imported_btn").removeClass("active");
                    $( "#import_route_btn" ).hide();
                    $("#teamwork_btn").addClass("active");
                    $( "#add_teamwork_btn" ).show();

                    $( "#imported" ).hide();
                    $( "#teamwork" ).show();
                } else {
                    $("#teamwork_btn").removeClass("active");
                    $( "#add_teamwork_btn" ).hide();
                    $("#imported_btn").addClass("active");
                    $( "#import_route_btn" ).show();

                    $( "#teamwork" ).hide();
                    $( "#imported" ).show();
                }
            }).trigger('hashchange');
        });
    </script>
@endsection
