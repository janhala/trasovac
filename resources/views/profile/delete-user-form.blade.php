<x-jet-action-section>
    <x-slot name="title">
        {{ __('Smazat účet') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Trvale smažte svůj účet.') }}
    </x-slot>

    <x-slot name="content">
        <div class="max-w-xl text-sm text-gray-600">
            {{ __('Jakmile bude váš účet smazán, budou trvale smazány všechny s ním spojená data. Před smazáním účtu si prosím uložte veškerá data nebo informace, které si chcete ponechat.') }}
        </div>

        <div class="mt-5">
            <x-jet-danger-button class="btn btn-danger btn-raised" wire:click="confirmUserDeletion" wire:loading.attr="disabled">
                {{ __('Smazat účet') }}
            </x-jet-danger-button>
        </div>

        <!-- Delete User Confirmation Modal -->
        <x-jet-dialog-modal wire:model="confirmingUserDeletion">
            <x-slot name="title">
                {{ __('Smazat účet') }}
            </x-slot>

            <x-slot name="content">
                {{ __('Opravdu chcete smazat svůj účet? Jakmile bude váš účet smazán, budou trvale smazány všechna data s ním spojená. Zadejte své heslo a potvrďte, že chcete trvale smazat svůj účet.') }}

                <div class="mt-4 form-group" x-data="{}" x-on:confirming-delete-user.window="setTimeout(() => $refs.password.focus(), 250)">
                    <x-jet-label for="password" value="{{ __('Heslo') }}" class="bmd-label-static" />
                    <x-jet-input type="password" class="mt-1 block w-3/4 form-control"
                                x-ref="password"
                                wire:model.defer="password"
                                wire:keydown.enter="deleteUser" />

                    <x-jet-input-error for="password" class="mt-2" />
                </div>
            </x-slot>

            <x-slot name="footer">
                <x-jet-secondary-button class="btn btn-default" wire:click="$toggle('confirmingUserDeletion')" wire:loading.attr="disabled">
                    {{ __('Zrušit') }}
                </x-jet-secondary-button>

                <x-jet-danger-button class="ml-2 btn btn-danger btn-raised" wire:click="deleteUser" wire:loading.attr="disabled">
                    {{ __('Smazat účet') }}
                </x-jet-danger-button>
            </x-slot>
        </x-jet-dialog-modal>
    </x-slot>
</x-jet-action-section>
