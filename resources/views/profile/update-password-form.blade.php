<x-jet-form-section submit="updatePassword">
    <x-slot name="title">
        {{ __('Aktualizovat heslo') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Volte dlouhé, náhodné heslo - jen tak zůstane váš účet v bezpečí.') }}
    </x-slot>

    <x-slot name="form">
        <div class="col-span-6 sm:col-span-4 form-group">
            <x-jet-label  for="current_password" class="bmd-label-static" value="{{ __('Současné heslo') }}" />
            <x-jet-input id="current_password" type="password" class="mt-1 block w-full form-control" wire:model.defer="state.current_password" autocomplete="current-password" />
            <x-jet-input-error for="current_password" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4 form-group">
            <x-jet-label for="password" class="bmd-label-static" value="{{ __('Nové heslo') }}" />
            <x-jet-input id="password" type="password" class="mt-1 block w-full form-control" wire:model.defer="state.password" autocomplete="new-password" />
            <x-jet-input-error for="password" class="mt-2" />
        </div>

        <div class="col-span-6 sm:col-span-4 form-group">
            <x-jet-label for="password_confirmation" class="bmd-label-static" value="{{ __('Zopakujte nové heslo') }}" />
            <x-jet-input id="password_confirmation" type="password" class="mt-1 block w-full form-control" wire:model.defer="state.password_confirmation" autocomplete="new-password" />
            <x-jet-input-error for="password_confirmation" class="mt-2" />
        </div>
    </x-slot>

    <x-slot name="actions">
        <x-jet-action-message class="mr-3" on="saved">
            {{ __('Úspěšně změněno.') }}
        </x-jet-action-message>

        <x-jet-button class="btn btn-primary btn-raised">
            {{ __('Uložit') }}
        </x-jet-button>
    </x-slot>
</x-jet-form-section>
