@extends('layouts.app')

@section('title', 'O aplikaci')

@section('head')
    <style>
        #about ul li {
            list-style-type:disc;
        }
    </style>
@endsection

@section('content')
    <div class="row" id="about">
        <div class="container mt-3">
            <h3>Trasovač</h3>
            <p class="pl-1 pr-1 font-italic">
                <q>Tvořte a sdílejte turisticky zajímavá místa a trasy.</q>
            </p>
            <p>Autorem aplikace je <a href="https://www.linkedin.com/in/janhala/" target="_blank">Jan Hála</a>.</p>
            <hr />
            <h5>Stěžejní funkcionalita aplikace</h5>
            @guest
                <p class="font-italic">Pro získání plné funkcionality se prosím přihlašte.</p>
            @endguest
            <ul class="pl-4">
                <li>Vytváření tras a míst a jejich sdílení.</li>
                <li>Trasy lze označit jako neveřejné a sdílet pomocí speciálního odkazu či číselného kódu.</li>
                <li>U tras je možné povolit spolupráci a zaslat je svým spolupracovníkům, kteří je poté mohou editovat.</li>
                <li>Trasy lze procházet pomocí navigace po trase.</li>
                <li>K jednotlivým bodům trasy je možné přidávat obsah (obrázek, text, zvuk), zobrazující se při navštívení bodu.</li>
                <li>K bodům je také možné přiřadit kvíz ověřující znalosti uživatele procházejícího trasu.</li>
            </ul>
            <h5>Zřeknutí se odpovědnosti</h5>
            <p>
                Aplikace obsahuje místa a trasy tvořené komunitou uživatelů.
                Uživateli používajícímu aplikaci je umožněno vydat se po libovolné trase, autor aplikace však nijak
                negarantuje její správnost a uživatel se tak na trasu vydává na svou vlastní zodpovědnost.
            </p>
            <p>
                Veškeré uvedené údaje v aplikaci, stejně jako vykreslené trasy jsou pouze orientační a nemusejí
                odpovídat skutečnosti. Před projitím trasy pamatujte prosím na to, že vzdálenost mezi body trasy
                je vypočtena vzdušnou čarou a ve skutečnosti tedy bude obvykle vyšší.
            </p>
            <h5>Pro vývojáře</h5>
            <p>
                Aplikace byla vyvinuta jako součást bakalářské práce, a to pod licencí MIT
                - zdrojový kód tedy najdete <a href="https://gitlab.com/janhala/trasovac" target="_blank">v příslušném repozitáři</a>.
            </p>
            <h5>Použité komponenty třetích stran</h5>
            <ul class="pl-4">
                <li><a href="https://mdbootstrap.github.io/bootstrap-material-design/docs/4.0/about/overview/" target="_blank">Material Design for Bootstrap v4</a> (MIT)</li>
                <li><a href="https://www.polonel.com/snackbar/" target="_blank">SnackBar</a> (MIT)</li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')

@endsection
