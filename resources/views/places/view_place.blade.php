@extends('layouts.app')

@section('title', 'Zobrazit místo')

@section('head')
    <style>
        .lat-lng-wrapper {
            display: none;
        }

        #map {
            position: relative;
            overflow: hidden;
            height: 210px;
        }

        @if($favorite > 0)
            #add_to_favorites_btn {
                display: none;
            }
        @else
            #remove_from_favorites_btn {
                display: none;
            }
        @endif
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="container p-0">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mb-0" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div id="map"></div>
            <div class="row map-buttons">
                <div class="col-sm w-50">
                    <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="go_back_btn">
                        <i class="material-icons">arrow_back</i>
                    </button>
                </div>
                <div class="col-sm w-50 text-right">
                    @if (isset(Auth::user()->id) && Auth::user()->id !== $place->user_id)
                        <div class="col p-0">
                            <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="add_to_favorites_btn">
                                <i class="material-icons">favorite_border</i>
                            </button>
                            <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="remove_from_favorites_btn">
                                <i class="material-icons">favorite</i>
                            </button>
                        </div>
                    @endif
                    <div class="col p-0">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="share_button" data-toggle="modal" data-target="#CopyURLModal">
                            <i class="material-icons">share</i>
                        </button>

                        <div class="modal fade pt-5" id="CopyURLModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Sdílet místo</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="container pb-3">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="row">
                                                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto">
                                                            <i class="material-icons" id="share_on_facebook_btn">facebook</i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="row">
                                                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto">
                                                            <i class="material-icons" id="share_by_email_btn">mail</i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <form class="m-0">
                                            <input type="text" class="form-control" id="input_with_url" name="input_with_url" />
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                                        <button type="button" class="btn btn-primary" onclick="copyToClipboard();">Zkopírovat URL</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col p-0">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-right" id="show_place_btn">
                            <i class="material-icons">place</i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="container pt-3 pl-3 pr-3">
            <div class="row">
                <div class="col-sm w-50">
                    <div class="row">
                        <h4>
                            {{ \Illuminate\Support\Str::limit($place->name, 30, $end='...') }}
                        </h4>
                    </div>
                    <div class="row">
                        <p>
                            {{ \Illuminate\Support\Str::limit($place->address, 50, $end='...') }}
                        </p>
                    </div>
                </div>
                <div class="col-sm w-50">
                    <div class="row">
                        <button type="button" class="btn btn-success bmd-btn-fab mx-auto" id="navigate_btn">
                            <i class="material-icons">directions</i>
                        </button>
                    </div>
                    <div class="row">
                        <p class="mx-auto">
                            NAVIGOVAT
                        </p>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        @if ($place->entrance_fee)
                            <div class="mx-auto">
                                <div class="row">
                                    <span class="mx-auto"><b>Vstupné</b></span>
                                </div>
                                <div class="row">
                                    <p class="mx-auto">{{$place->entrance_fee}} Kč</p>
                                </div>
                            </div>
                        @endif

                        @if (!isset(Auth::user()->id) || isset(Auth::user()->id) && Auth::user()->id !== $place->user_id)
                                <div class="mx-auto">
                                    <div class="row">
                                        <span class="mx-auto"><b>Autorem místa je</b></span>
                                    </div>
                                    <div class="row">
                                        <p class="mx-auto">{{$place->author->first_name}} {{$place->author->surname}}</p>
                                    </div>
                                </div>
                        @endif
                    </div>
                </div>
                <div class="container pb-3">
                    <div class="row">
                        @if (isset(Auth::user()->id) && $place->user_id === Auth::user()->id)
                            <div class="col text-center">
                                <form method="POST" action="{{ route('update_place', ['id' => $place->id]) }}">
                                    @csrf
                                    <button type="submit" class="btn btn-raised btn-success">
                                        <i class="fas fa-pencil-alt"></i> Upravit
                                    </button>
                                </form>
                            </div>
                            <div class="col text-center">
                                <form method="POST" action="{{ route('delete_place', ['id' => $place->id]) }}">
                                    @method('delete')
                                    @csrf
                                    <button type="submit" class="btn btn-raised btn-danger">
                                        <i class="fas fa-trash"></i> Smazat
                                    </button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="container">
                    <p>{{$place->description}}</p>
                </div>

                <div class="container reviews">
                    <p class="mb-0"><b>Recenze místa:</b></p>

                    @if ($reviews->isNotEmpty())
                        @if (isset(Auth::user()->id) && $review->isNotEmpty())
                            <p class="mt-2 font-italic">Toto místo jste již hodnotil.</p>
                        @elseif (isset(Auth::user()->id) && Auth::user()->id !== $place->user_id)
                            <div class="col mt-2">
                                <button type="button" class="btn btn-raised btn-success" data-toggle="modal" data-target="#add_review_modal"><i class="fas fa-star"></i> Napsat recenzi</button>
                            </div>
                        @else
                            <p class="mt-2 font-italic">Jako autor nemůžete místo hodnotit.</p>
                        @endif
                        <ul class="list-group">
                            @foreach($reviews as $review)
                                <li>
                                    <a class="list-group-item">
                                        <div>
                                            @for($x = 0; $x < $review->number_of_stars; $x++)
                                                <i class="material-icons">star</i>
                                            @endfor
                                        </div>
                                        <div class="bmd-list-group-col pl-2">
                                            <p class="list-group-item-heading pt-3">{{$review->users->first_name}} {{$review->users->last_name}}</p>
                                            <p class="list-group-item-text">
                                                {{$review->comment}}
                                            </p>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        @if (isset(Auth::user()->id) && Auth::user()->id !== $place->user_id)
                            <div class="col mt-2">
                                <button type="button" class="btn btn-raised btn-success" data-toggle="modal" data-target="#add_review_modal"><i class="fas fa-star"></i> Napsat recenzi</button>
                            </div>
                        @else
                            <p class="mt-2 font-italic">Jako autor nemůžete místo hodnotit.</p>
                        @endif

                        <p class="mt-2">Zatím tu žádná recenze není.</p>
                    @endif
                </div>
            </div>
        </div>

        <!-- add_review_modal -->
        <div class="modal fade" id="add_review_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form id="review_form" method="POST" action="{{ route('add_new_review', ['place_id' => $place->id]) }}">
                        @csrf

                        <div class="modal-header">
                            <h5 class="modal-title">Recenze místa</h5>
                        </div>

                        <div class="modal-body">
                            <div class="form-group">
                                <label for="number_of_stars" class="bmd-label-static">Počet hvězdiček</label>
                                <select class="form-control" id="number_of_stars" name="number_of_stars" required>
                                    <option value="1">1 (nejméně)</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5 (nejvíce)</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="comment" class="bmd-label-static">Text recenze</label>
                                <textarea class="form-control" id="comment" name="comment" rows="3" required></textarea>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Zavřít</button>
                            <button type="submit" class="btn btn-primary">Uložit recenzi</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection

@section('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&callback=initMap&libraries=&v=weekly"
        defer
    ></script>

    <script>
        function initMap() {
            const lat = {{$place->lat}};
            const lng = {{$place->lng}};
            var location = { lat: lat, lng: lng };

            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 11,
                center: location,
                gestureHandling: 'none',
                zoomControl: false,
                disableDefaultUI: true
            });

            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
        }

        function copyToClipboard() {
            var input = document.getElementById("input_with_url");
            input.select();
            input.setSelectionRange(0, 99999)
            document.execCommand("copy");
        }

        function showPlace() {
            window.open('http://maps.google.com/?q={{$place->lat}},{{$place->lng}}');
        }

        $( document ).ready(function() {
            $( "#map" ).click(function() {
                showPlace();
            });

            $( "#show_place_btn" ).click(function() {
                showPlace();
            });

            $( "#share_button" ).click(function() {
                var input = document.getElementById("input_with_url");
                input.value = window.location.href;
            });

            $( "#share_on_facebook_btn" ).click(function() {
                window.open('https://www.facebook.com/sharer/sharer.php?u=' + window.location.href);
            });

            $( "#go_back_btn" ).click(function() {
                window.history.back();
            });

            $( "#navigate_btn" ).click(function() {
                window.open('http://maps.google.com/maps?daddr={{$place->lat}},{{$place->lng}}');
            });

            $( "#share_by_email_btn" ).click(function() {
                Snackbar.show({
                    text: 'Upozornění - sdílení pomocí emailu funguje pouze při použití některých emailových klientů (Gmail, Pošta).',
                    actionText: 'OK',
                    actionTextColor: '#4CAF50',
                    pos: 'bottom-center',
                    onClose: function() {
                        window.open('mailto:?subject=Objevil jsem nové místo v aplikaci Trasovač.com&body=Místo má název "{{ \Illuminate\Support\Str::limit($place->name, 35, $end='...') }}". Podívej se na něj pomocí url: ' + window.location.href);
                    }
                });
            });

            @if (isset(Auth::user()->id))
                $( "#add_to_favorites_btn" ).click(function() {
                    $.post( "{{ route('new_favorite_place') }}", { user_id: {{Auth::user()->id}}, place_id: {{$place->id}} })
                        .done(function( message ) {
                            if (message === 'success') {
                                $( "#add_to_favorites_btn" ).hide();
                                $( "#remove_from_favorites_btn" ).show();
                            }
                        });
                });
            @endif

            @if (isset(Auth::user()->id))
                $( "#remove_from_favorites_btn" ).click(function() {
                    $.ajax({
                        method: "DELETE",
                        url: "{{ route('remove_favorite_place') }}",
                        data: { user_id: {{Auth::user()->id}}, place_id: {{$place->id}} }
                    })
                        .done(function( message ) {
                            if (message === 'success') {
                                $( "#remove_from_favorites_btn" ).hide();
                                $( "#add_to_favorites_btn" ).show();
                            }
                        });
                });
            @endif
        });
    </script>
@endsection
