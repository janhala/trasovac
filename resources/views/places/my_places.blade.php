@extends('layouts.app')

@section('title', 'Tvorba míst')

@section('content')
    <div class="row">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            @if(Session::has('message'))
                <div class="alert alert-success mt-3" role="alert">
                    {{ Session::get('message')  }}
                </div>
            @endif

            <div class="col-auto mt-3">
                <span>Vytvořeno míst: {{$places_count}}</span>
            </div>

            <div class="list-group">
                @foreach($places as $place)
                    <a href="{{route('view_place', $place->id)}}" class="list-group-item list-group-item-action" aria-current="true">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">
                                {{$place->name}}
                            </h5>
                            <small class="pt-1 text-muted">přidáno {{date('d.m.Y', strtotime($place->created_at))}}</small>
                        </div>
                        <p class="mt-1 mb-1 w-100">
                            {{ \Illuminate\Support\Str::limit($place->description, 150, $end='...') }}
                        </p>
                        <div class="d-flex flex-row w-100">
                            @if ($place->address)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="fas fa-map-marker-alt"></i>
                                        {{$place->address}}
                                    </span>
                                </div>
                            @endif

                            @if ($place->entrance_fee)
                                <div class="p-2 mx-auto">
                                    <span>
                                        <i class="far fa-money-bill-alt"></i>
                                        {{$place->entrance_fee}} Kč
                                    </span>
                                </div>
                            @endif
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row">
        <div class="container">
            <button type="button" class="btn btn-success bmd-btn-fab add-route-btn">
                <i class="material-icons">add</i>
            </button>
        </div>
    </div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $( document ).ready(function() {
            $( ".add-route-btn" ).click(function() {
                window.location.href = "{{ route('create_place')}}";
            });
        });
    </script>
@endsection
