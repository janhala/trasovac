@extends('layouts.app')

@if (isset($place))
    @section('title', 'Upravit místo')
@else
    @section('title', 'Přidat nové místo')
@endif

@section('head')
    <style>
        .lat-lng-wrapper {
            display: none;
        }

        #map {
            height: 200px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="container">
            @if($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger mt-2" role="alert">
                        {{ $error }}
                    </div>
                @endforeach
            @endif

            <form class="pt-2" method="POST" @if (isset($place))action="{{ route('update_place_submit', ['id' => $place->id]) }}" @else action="{{ route('create_place_submit') }}"@endif>
                @if (isset($place))
                    @method('put')
                @endif
                @csrf
                <div class="form-group">
                    <label for="name" class="bmd-label-static">Název</label>
                    <input type="text" class="form-control" name="name" id="name" required @if (isset($place) && $place->name)value="{{$place->name}}"@endif>
                    <small class="text-muted">Zadejte název místa.</small>
                </div>

                <div class="form-group">
                    <label for="description" class="bmd-label-static">Popis</label>
                    <textarea class="form-control" name="description" id="description" rows="3" required>@if (isset($place) && $place->description){{$place->description}}@endif</textarea>
                    <small class="text-muted">Zadejte popis místa.</small>
                </div>

                <div class="switch">
                    <label>
                        <input type="checkbox" name="address_or_coordination" id="address_or_coordination">
                        Namísto adresy zadat souřadnice, či vybrat pozici z mapy.
                    </label>
                </div>

                <div class="form-group" id="address_wrapper">
                    <label for="address" class="bmd-label-static">Adresa</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="" required @if (isset($place) && $place->address)value="{{$place->address}}"@endif>
                    <small class="text-muted">Pomocí nápovědy vyberte adresu místa. Pro zobrazení nápovědy začněte do pole psát název místa, či jeho adresu.</small>
                </div>

                <div class="lat-lng-wrapper" id="lat_lng_wrapper">
                    <div class="form-group">
                        <label for="lat" class="bmd-label-static">Zeměpisná šířka (lat)</label>
                        <input type="text" class="form-control" name="lat" id="lat" required @if (isset($place) && $place->lat)value="{{$place->lat}}"@endif>
                        <small class="text-muted">Zadejte zeměpisnou šířku místa.</small>
                    </div>

                    <div class="form-group">
                        <label for="lng" class="bmd-label-static">Zeměpisná délka (lng)</label>
                        <input type="text" class="form-control" name="lng" id="lng" required @if (isset($place) && $place->lng)value="{{$place->lng}}"@endif>
                        <small class="text-muted">Zadejte zeměpisnou délku místa.</small>
                    </div>

                    <div class="form-group">
                        <label class="bmd-label-static">Výběr pozice z mapy</label>
                        <div id="map"></div>
                        <small class="text-muted">Pokud neznáte příslušné souřadnice, můžete je vybrat přímo z mapy.</small>
                    </div>
                </div>

                <div class="form-group">
                    <label for="entrance_fee" class="bmd-label-static">Vstupné</label>
                    <input type="number" class="form-control" name="entrance_fee" id="entrance_fee" min="0" max="10000" @if (isset($place) && $place->entrance_fee)value="{{$place->entrance_fee}}"@endif>
                    <small class="text-muted">Zadejte výši vstupného, které je nutné pro navštívení tohoto místa uhradit, a to v Kč.</small>
                </div>

                <button class="btn btn-default" id="cancel_btn" onclick="goBack(); return false;">Zrušit</button>
                <button type="submit" class="btn btn-primary btn-raised">Uložit</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAWjmVLygLHVIIYxMQ-Moq9_5VogJ7Wxqo&libraries=places&callback=initAutocomplete">
    </script>

    <script>
        function initAutocomplete() {
            initMap();

            var input = document.getElementById('address');
            var autocomplete = new google.maps.places.Autocomplete(input);
            autocomplete.setFields(['geometry']);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();

                if (!place.geometry) {
                    window.alert("Žádné data o poloze '" + place.name + "' nebyla nalezena.");
                    return;
                }

                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();

                $("input[name=lat]").val(lat);
                $("input[name=lng]").val(lng);
            });
            autocomplete.setOptions({
                componentRestrictions: {country: ['CZ', 'SK']}
            });
        }

        function initMap() {
            const myLatlng = { lat: 49.876, lng: 15.493 };
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 6,
                center: myLatlng,
                disableDefaultUI: true,
                fullscreenControl: true,
                zoomControl: true
            });

            let infoWindow = new google.maps.InfoWindow({
                content: "Klikněte na mapě na místo, jehož souřadnice chcete získat.",
                position: myLatlng,
            });
            infoWindow.open(map);

            map.addListener("click", (mapsMouseEvent) => {

                infoWindow.close();

                infoWindow = new google.maps.InfoWindow({
                    position: mapsMouseEvent.latLng,
                });
                infoWindow.setContent(
                    JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
                );
                infoWindow.open(map);

                const lat = mapsMouseEvent.latLng.toJSON().lat;
                const lng = mapsMouseEvent.latLng.toJSON().lng;

                $("#lat").val(lat);
                $("#lng").val(lng);
            });
        }
    </script>

    <script type="text/javascript">
        function goBack() {
            window.location.href = "{{ route('my_places')}}";
        }

        $( document ).ready(function() {
            @if(isset($place) && !isset($place->address))
                $( "#address_or_coordination" ).prop( "checked", true );
                $("#address_wrapper").hide();
                $("#lat_lng_wrapper").show();
            @endif

            $( "#address_or_coordination" ).change(function() {
                if ($( "#address_or_coordination" ).prop('checked')) {
                    $("#address").prop('required', false);

                    $("#address_wrapper").hide();
                    $("#lat_lng_wrapper").show();
                } else {
                    $("#address").prop('required', true);

                    $("#address_wrapper").show();
                    $("#lat_lng_wrapper").hide();
                }
            });

            $( "#address_or_coordination" ).click(function() {
                $("#address").val('');
                $("#lat_lng_wrapper input").val('');
            });

            $(window).keydown(function(event){
                if(event.keyCode === 13) {
                    event.preventDefault();
                    return false;
                }
            });
        });
    </script>
@endsection
