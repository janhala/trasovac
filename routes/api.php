<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/**
 * Routes
 */

Route::get('/app/routes/navigation/get_contents/{point_id}', [Controllers\ContentController::class, 'getContents'])->name('get_contents');

Route::get('/app/routes/navigation/get_quiz/{point_id}', [Controllers\QuizController::class, 'getQuiz'])->name('get_quiz');

Route::post('/app/routes/navigation/check_quiz_answers/{user_id}/{point_id}', [Controllers\QuizController::class, 'checkAnswers'])->name('check_quiz_answers');

Route::post('/app/routes/navigation/arrived_to_point/{user_id}/{point_id}', [Controllers\RouteController::class, 'arrivedToPoint'])->name('arrived_to_point');

Route::post('/app/routes/new_favorite_route', [Controllers\FavoriteController::class, 'newFavoriteRoute'])->name('new_favorite_route');

Route::delete('/app/routes/remove_favorite_route', [Controllers\FavoriteController::class, 'removeFavoriteRoute'])->name('remove_favorite_route');

/**
 * Places
 */

Route::post('/app/places/new_favorite_place', [Controllers\FavoriteController::class, 'newFavoritePlace'])->name('new_favorite_place');

Route::delete('/app/places/remove_favorite_place', [Controllers\FavoriteController::class, 'removeFavoritePlace'])->name('remove_favorite_place');
