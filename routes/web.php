<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', function () {
    return view('about');
})->name('about_app');

/**
 * Trasy
 */

Route::get('/', [Controllers\RouteController::class, 'browseRoutes'])->name('browse_routes');

Route::post('/', [Controllers\RouteController::class, 'browseRoutesWithFilters'])->name('browse_routes_with_filters');

Route::get('/route/{id}', [Controllers\RouteController::class, 'view'])->name('view_route');

Route::get('/route/navigation/{id}/{point_id}', [Controllers\RouteController::class, 'startNavigation'])->name('start_nav_without_useracc');

/**
 * Místa
 */

Route::get('/place/{id}', [Controllers\PlaceController::class, 'view'])->name('view_place');

/**
 * K následujícím routám mají přístup pouze přihlášení uživatelé
 */

Route::middleware(['auth'])->group(function () {
    Route::get('/profile', function () {
        return view('profile.profile');
    })->name('profile_settings');

    /**
     * Trasy a místa
     */

    Route::get('/favorites', [Controllers\FavoriteController::class, 'getFavouriteRoutesAndPlaces'])->name('favorite_places');

    /**
     * Trasy
     */

    Route::get('/my_routes', [Controllers\RouteController::class, 'myRoutes'])->name('my_routes');

    Route::get('/my_routes/create_route', [Controllers\RouteController::class, 'createOrUpdateRoute'])->name('create_route');

    Route::post('/my_routes/create_route/submit', [Controllers\RouteController::class, 'store'])->name('create_route_submit');

    Route::get('/my_routes/update_route/{id}', [Controllers\RouteController::class, 'createOrUpdateRoute'])->name('update_route');

    Route::post('/my_routes/update_route/{id}', [Controllers\RouteController::class, 'createOrUpdateRoute'])->name('update_route');

    Route::put('/my_routes/update_route/submit/{id}', [Controllers\RouteController::class, 'store'])->name('update_route_submit');

    Route::delete('/my_routes/delete_route/submit/{id}', [Controllers\RouteController::class, 'delete'])->name('delete_route');

    Route::get('/imported_routes', [Controllers\RouteController::class, 'importedRoutes'])->name('imported_routes');

    Route::get('/import_route/{url}/{route_id}', [Controllers\RouteController::class, 'importRouteUsingLink'])->name('import_route');

    Route::post('/imported_routes/import_route_num_code', [Controllers\RouteController::class, 'importRouteUsingNumericCode'])->name('import_route_num_code');

    Route::get('/teamwork/{url}/{route_id}', [Controllers\RouteController::class, 'importRouteForTeamwork'])->name('teamwork');

    Route::post('/routes/add_new_review', [Controllers\ReviewController::class, 'addOrUpdateReview'])->name('add_new_review');

    Route::get('/route/navigation/{id}', [Controllers\RouteController::class, 'startNavigation'])->name('start_navigation');

    /**
     * Místa
     */

    Route::get('/my_places', [Controllers\PlaceController::class, 'myPlaces'])->name('my_places');

    Route::get('/my_places/create_place', [Controllers\PlaceController::class, 'createOrUpdatePlace'])->name('create_place');

    Route::post('/my_places/create_place/submit', [Controllers\PlaceController::class, 'store'])->name('create_place_submit');

    Route::get('/my_places/update_place/{id}', [Controllers\PlaceController::class, 'createOrUpdatePlace'])->name('update_place');

    Route::post('/my_places/update_place/{id}', [Controllers\PlaceController::class, 'createOrUpdatePlace'])->name('update_place');

    Route::put('/my_places/update_place/submit/{id}', [Controllers\PlaceController::class, 'store'])->name('update_place_submit');

    Route::delete('/my_places/delete_place/submit/{id}', [Controllers\PlaceController::class, 'delete'])->name('delete_place');
});

